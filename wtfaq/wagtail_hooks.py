from wagtail.contrib.modeladmin.options import ModelAdmin,  modeladmin_register

from .models import Faq

class FaqAdmin(ModelAdmin):
    model = Faq
    menu_label = 'FAQ'
    menu_icon = 'doc-full'
    list_display = ('question',  'sort_order')
    search_fields = ('question', 'answer')
    menu_order = 431
    add_to_settings_menu = False

modeladmin_register(FaqAdmin)
