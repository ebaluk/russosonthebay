from django.db import models
from django import forms

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel


from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtblocks.models import ContentBlockThemed
from wtforms.models import WtFormLinkField

from django.utils.safestring import mark_safe
from django.utils.html import format_html

from wtpages.models import *

class Faq(models.Model):
    class Meta:
        verbose_name = _('FAQ')
        verbose_name_plural = _('FAQs')
        ordering = ['sort_order']
    
    question = models.CharField(max_length=255)
    answer = RichTextField(blank=True)   
    sort_order = models.IntegerField(default=0)
    visible = models.BooleanField(blank=True, default=1)

    panels = [
        MultiFieldPanel([
            FieldPanel('question'),
            FieldPanel('answer' ),
            FieldPanel('visible' ),
            FieldPanel('sort_order' ),                        
        ],heading="Basic",),
    ]
        
    def __str__(self):
        return self.question
    
class FaqItem(Faq, Orderable):
    page = ParentalKey('FaqPage', related_name='faq_items')
    panels = [
        MultiFieldPanel([
            FieldPanel('question'),
            FieldPanel('answer' ),            
            FieldPanel('visible' ),            
        ],heading="Basic",),
    ]          
    
class FaqPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('FaqPage', related_name='carousel_items')
    
class FaqPageSideCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('FaqPage', related_name='side_carousel_items')
    
class FaqPageBottomCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('FaqPage', related_name='bottom_carousel_items')    
        
class FaqPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('FaqPage', related_name='related_links')
    
class FaqPageFormLink(Orderable, WtFormLinkField):
    page = ParentalKey('FaqPage', related_name='form_links')




class FaqPage(WtBasePage, LayoutablePage):
    class Meta:
        verbose_name = _("FAQ Page")
    
    template = 'wtfaq/faq_page.html'
    inline_template = 'wtfaq/faq_page_inline.html'
    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    body = RichTextField(blank=True, verbose_name = _("Text"))
    
    hide_title = models.BooleanField(default=False)
    show_index_title = models.BooleanField(default=False)    
    
    carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    side_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    bottom_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    subpage_types = []
        
    use_shared_faq = models.BooleanField(default=True)
    
    @property
    def faqs(self):
        return Faq.objects.filter(visible=True) if self.use_shared_faq else self.faq_items.filter(visible=True)
    
    
    @property
    def navbar_items(self):
        return self.get_parent().get_children().live().in_menu()
    
    @property
    def top_page_title(self):        
        return self.get_parent().title
    
    @property
    def top_page(self):
        return self.get_parent()
        
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('page_heading', classname="full title"),
            FieldPanel('page_content_heading', classname="full title"),
            
            FieldPanel('page_layout'),
            FieldPanel('body'),            
            FieldPanel('show_index_title'),
            FieldPanel('hide_title'),
            FieldPanel('show_separator'),
            
            FieldPanel('use_shared_faq'),
            
        ],heading=_('Basic')),

        MultiFieldPanel([InlinePanel('faq_items'),],heading=_("FAQ"),classname="collapsible collapsed"),
        MultiFieldPanel([
            FieldPanel('carousel_style'),
            InlinePanel('carousel_items'),],heading=_('Top Images'),classname="collapsible collapsed"),
        MultiFieldPanel([
            FieldPanel('side_carousel_style'),
            InlinePanel('side_carousel_items'),],heading=_('Side Images'),classname="collapsible collapsed"),        
        MultiFieldPanel([
            FieldPanel('bottom_carousel_style'),
            InlinePanel('bottom_carousel_items'),],heading=_('Bottom Images'),classname="collapsible collapsed"),              
        MultiFieldPanel([InlinePanel('related_links'),], heading=_('Related Links'), classname="collapsible collapsed"),
        MultiFieldPanel([InlinePanel('form_links'),], heading=_('Forms'), classname="collapsible collapsed"),
    ]
    
    promote_panels = [
        MultiFieldPanel([            
            FieldPanel('slug'),
            FieldPanel('show_in_menus'),
             FieldPanel('seo_title'),
             FieldPanel('search_description'),
             FieldPanel('seo_keywords'),
        ], _('Common page configuration')),
    ]
