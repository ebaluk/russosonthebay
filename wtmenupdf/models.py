from django.db import models
from django.utils.translation import ugettext_lazy as _
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailcore.models import Orderable
from modelcluster.fields import ParentalKey 

from wtpages.models import *

class MenuPdf(models.Model):
    class Meta:
        verbose_name = _('Menu')
        verbose_name_plural = _('Menus')
    
    title = models.CharField(max_length=255)    
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )    
    link_document = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        related_name='+'
    )
    visible = models.BooleanField(blank=True, default=1)    
    panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            ImageChooserPanel('image'),
            DocumentChooserPanel('link_document'),
        ],heading="Basic",),
                        
    ]
    
    @property
    def link(self):
        return self.link_document.url if self.link_document else '#' 
    
        
    def __str__(self):
        return self.title
    
class MenuPdfLinksPagelItem(Orderable):
    page = ParentalKey('MenuPdfLinksPage', related_name='menu_items')
    menu = models.ForeignKey(
        'MenuPdf',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='+',
        #help_text="Special Events, Meetings, etc. Sub-Page",   
    )

class MenuPdfLinksPage(WtBasePage):
    class Meta:
        verbose_name = _('Menu Thumbnails Page ( for Special Events, Meetings, etc. )')
    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")    
    body = RichTextField(blank=True, help_text="Description")
    subpage_types = []
    template = 'wtmenupdf/menu_pdf_links_page.html'
    inline_template = 'wtmenupdf/menu_pdf_links_page_inline.html'
    
    @property
    def menus(self):
        return self.menu_items.filter(menu__visible=True)
        
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title", ),    
            FieldPanel('page_heading', classname="full title", ),
            FieldPanel('page_content_heading', classname="full title"),
            FieldPanel('body'),                
        ],heading=_('Basic')),
        MultiFieldPanel([
            InlinePanel('menu_items'),],heading=_('Available Menus'),classname="collapsible"),                      
    ]
    

