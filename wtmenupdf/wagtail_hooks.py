from wagtail.contrib.modeladmin.options import ModelAdmin,  modeladmin_register

from .models import MenuPdf

class MenuPdfAdmin(ModelAdmin):
    model = MenuPdf
    menu_label = 'Menus'
    menu_icon = 'doc-full'
    list_display = ('title',  )
    search_fields = ('title')
    menu_order = 420
    add_to_settings_menu = False

modeladmin_register(MenuPdfAdmin)
