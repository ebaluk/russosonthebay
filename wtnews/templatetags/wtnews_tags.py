from datetime import date
from django import template
from django.conf import settings

from wtnews.models import NewsPage, NewsIndexPage

register = template.Library()

@register.inclusion_tag(
    'wtnews/tags/news_listing_homepage.html',
    takes_context=True
)
def news_listing_homepage(context, count=4):
    news = NewsPage.objects.live().order_by('-date')
    return {
        'news': news[:count].select_related('image'),
        'all_news': NewsIndexPage.objects.live().first(),#NewsIndexPage,
        # required by the pageurl tag that we want to use within this template
        'request': context['request'],
    }
