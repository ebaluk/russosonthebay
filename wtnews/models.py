# -*- coding: utf-8 -*-
from django.db import models
from django import forms

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel


from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtpages.models import CarouselItem, RelatedLink
from wtblocks.models import ContentBlockThemed
from wtpages.models import *



class NewsIndexPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('wtnews.NewsIndexPage', related_name='carousel_items')

class NewsIndexPage(WtBasePage):
    subpage_types = ['wtnews.NewsPage']
    @property
    def news(self):
        # Get list of live blog pages that are descendants of this page
        news = NewsPage.objects.live().descendant_of(self)

        # Order by most recent date first
        news = news.order_by('-date')

        return news

    def get_context(self, request):
        # Get news
        news = self.news

        # Filter by tag
        tag = request.GET.get('tag')
        if tag:
            news = news.filter(tags__name=tag)

        # Pagination
        page = request.GET.get('page')
        paginator = Paginator(news, 10)  # Show 10 news per page
        try:
            news = paginator.page(page)
        except PageNotAnInteger:
            news = paginator.page(1)
        except EmptyPage:
            news = paginator.page(paginator.num_pages)

        # Update template context
        context = super(NewsIndexPage, self).get_context(request)
        context['news'] = news
        return context

NewsIndexPage.content_panels = [
    FieldPanel('title', classname="full title"),
    MultiFieldPanel(
        [
            InlinePanel('carousel_items', label="Carousel items"),
        ],
        heading="Carousel items",
        classname="collapsible collapsed"
    ),
]



# News page
class NewsPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('wtnews.NewsPage', related_name='carousel_items')

class NewsPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('wtnews.NewsPage', related_name='related_links')

class NewsPageTag(TaggedItemBase):
    content_object = ParentalKey('wtnews.NewsPage', related_name='tagged_items')

class NewsPage(WtBasePage):
    date = models.DateField("News date")
    intro = models.TextField(help_text="Excerp")
    body = StreamField([
        ('bodylist', ContentBlockThemed(label=_("Content")))
    ])
    tags = ClusterTaggableManager(through=NewsPageTag, blank=True)

    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )

    subpage_types = []
    search_fields = Page.search_fields + (
        index.SearchField('body'),
        index.SearchField('intro'),
    )

    parent_page_types = ['wtnews.NewsIndexPage']
    @property
    def news_index(self):
        # Find closest ancestor which is a blog index
        return self.get_ancestors().type(NewsIndexPage).last()

    def __unicode__(self):
        return self.text


NewsPage.content_panels = [
    MultiFieldPanel(
        [
            FieldPanel('title', classname="full title"),
            FieldPanel('date',),
            FieldPanel('intro', classname="full title"),
            ImageChooserPanel('image'),
        ],
        heading="Basic",
    ),
    StreamFieldPanel('body'),
    MultiFieldPanel(
        [
            InlinePanel('carousel_items', label="Carousel items"),
        ],
        heading="Carousel items",
        classname="collapsible collapsed"
    ),
    MultiFieldPanel(
        [
            InlinePanel('related_links', label="Related links"),
        ],
        heading="Related links",
        classname="collapsible collapsed"
    ),
]

NewsPage.promote_panels = WtBasePage.promote_panels + [
    FieldPanel('tags'),
]
