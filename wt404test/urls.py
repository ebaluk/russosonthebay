from django.conf.urls import url
from .views import preview404 

urlpatterns = [
    url(r'^404.html$', preview404),        
]
