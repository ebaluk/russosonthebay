from __future__ import absolute_import, unicode_literals

from django import forms
from django.utils.text import capfirst
from django.utils.translation import ugettext as _
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, redirect
from django.contrib.admin.utils import quote, unquote

from wagtail.wagtailadmin import messages
from wagtail.wagtailcore.models import Site

from wagtail.contrib.modeladmin.views import WMABaseView, ModelFormView


class SiteSwitchForm(forms.Form):
    site = forms.ChoiceField(choices=[])

    class Media:
        js = [
            'wtmenulinks/js/site-switcher.js',
        ]

    def __init__(self, current_site, url_helper, **kwargs):
        initial = {'site': url_helper.get_action_url('edit', current_site.pk)}
        super(SiteSwitchForm, self).__init__(initial=initial, **kwargs)
        sites = []
        for site in Site.objects.all():
            sites.append((url_helper.get_action_url('edit', site.pk), site))
        self.fields['site'].choices = sites


