from copy import deepcopy
from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from modelcluster.models import ClusterableModel
from modelcluster.fields import ParentalKey

from wagtail.wagtailadmin.edit_handlers import (
    FieldPanel, PageChooserPanel, MultiFieldPanel, InlinePanel)
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.wagtailcore.models import Page, Orderable


from .app_settings import ACTIVE_CLASS
from .managers import MenuItemManager
from .panels import menupage_settings_panels

from wagtail.wagtailimages.models import Image  
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel


class MenuItem(models.Model):
    allow_subnav = False
        
    news_window = models.BooleanField(blank=True, default=False, verbose_name=_('Open Link in a new Window'), )

    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name = _("Image")
    )

    link_page = models.ForeignKey(
        'wagtailcore.Page',
        verbose_name=_('link to an internal page'),
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    
    link_document = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        related_name='+'
    )
    
    link_popup_form = models.ForeignKey(
        'wtforms.WtForm',
        null=True,
        blank=True,
        related_name='+',
    )
    
    link_url = models.CharField(
        max_length=255,
        verbose_name=_('link to a custom URL'),
        blank=True,
        null=True,
    )
    link_text = models.CharField(
        max_length=255,
        blank=True,
        help_text=_("Must be set if you wish to link to a custom URL."),
    )
    url_append = models.CharField(
        verbose_name=_("append to URL"),
        max_length=255,
        blank=True,
        help_text=(
            "Use this to optionally append a #hash or querystring to the "
            "above page's URL.")
    )

    objects = MenuItemManager()

    class Meta:
        abstract = True
        verbose_name = _("menu item")
        verbose_name_plural = _("menu items")

    def relative_url(self, current_site):
        try:
            
            if self.link_page:
                url = self.link_page.specific.relative_url(current_site)
            elif self.link_popup_form:
                url = "#modalForm-%s" % self.link_popup_form.pk
            elif self.link_document:
                url = self.link_document.url
            else:
                url = self.link_url            
            
        except AttributeError:
            url = self.link_url
        return url + self.url_append

    @property
    def menu_text(self):
        if self.link_page:            
            return self.link_text or self.link_page.title
        elif self.link_popup_form:            
            return self.link_text or self.link_popup_form.name
        elif self.link_document:
            return self.link_text or self.link_document.title
        return self.link_text

    def clean(self, *args, **kwargs):
        super(MenuItem, self).clean(*args, **kwargs)

        if self.link_url and not self.link_text:
            raise ValidationError({
                'link_text': [
                    _("This must be set if you're linking to a custom URL."),
                ]
            })

#         if not self.link_url and not ( self.link_page or self.link_document ):
#             raise ValidationError({
#                 'link_url': [
#                     _("This must be set if you're not linking to a page or document."),
#                 ]
#             })
# 
#         if self.link_url and self.link_page:
#             raise ValidationError(_(
#                 "You cannot link to both a page and URL. Please review your "
#                 "link and clear any unwanted values."
#             ))
#         
#         if self.link_url and self.link_document:
#             raise ValidationError(_(
#                 "You cannot link to both a document and URL. Please review your "
#                 "link and clear any unwanted values."
#             ))
#             
#             
#         if self.link_page and self.link_document:
#             raise ValidationError(_(
#                 "You cannot link to both a document and page. Please review your "
#                 "link and clear any unwanted values."
#             ))    

    def __str__(self):
        return self.menu_text

    panels = (
        ImageChooserPanel('image'),
        PageChooserPanel('link_page'),
        DocumentChooserPanel('link_document'),
        FieldPanel('url_append'),
        FieldPanel('link_url'),
        FieldPanel('link_text'),
        FieldPanel('link_popup_form'),
        FieldPanel('allow_subnav'),
        FieldPanel('news_window'),
        
    )


class FlatMenu(ClusterableModel):
    site = models.ForeignKey(
        'wagtailcore.Site',
        related_name="flat_menus")
    title = models.CharField(
        max_length=255,
        help_text=_("For internal reference only."))
    handle = models.SlugField(
        max_length=100,
        help_text=_(
            "Used to reference this menu in templates etc. Must be unique "
            "for the selected site."))
    heading = models.CharField(
        max_length=255,
        blank=True,
        help_text=_(
            "If supplied, appears above the menu when rendered."))

    class Meta:
        unique_together = ("site", "handle")
        verbose_name = _("flat menu")
        verbose_name_plural = _("flat menus")

    def __str__(self):
        return '%s (%s)' % (self.title, self.handle)

    panels = (
        MultiFieldPanel(
            heading=_("Settings"),
            children=(
                FieldPanel('site'),
                FieldPanel('title'),
                FieldPanel('handle'),
                FieldPanel('heading'),
            )
        ),
        InlinePanel('menu_items', label=_("Menu items")),
    )



class FlatMenuItem(Orderable, MenuItem):
    menu = ParentalKey('FlatMenu', related_name="menu_items")
    allow_subnav = models.BooleanField(
        default=False,
        verbose_name=_("allow sub-menu for this item"),
        help_text=_(
            "NOTE: The sub-menu might not be displayed, even if checked. "
            "It depends on how the menu is used in this project's templates."
        )
    )
