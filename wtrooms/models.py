# -*- coding: utf-8 -*-
from django.db import models
from django import forms

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel


from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtpages.models import CarouselItem, RelatedLink
from wtblocks.models import ContentBlockThemed
from wtpages.models import *
import re


class RoomIndexPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('wtrooms.RoomIndexPage', related_name='carousel_items')
class RoomItem(Orderable, models.Model):
    page = ParentalKey('wtrooms.RoomIndexPage', related_name='room_items')
    title = models.CharField(max_length=255)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name = _("Image")
    )
    body = RichTextField(blank=True, verbose_name=_('Description'))
    theme = models.ForeignKey(
        'wtpages.CssTheme',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name = _("Theme")
    )
    panels = [
        FieldPanel('title'),
        ImageChooserPanel('image'),
        FieldPanel('body'),
        #FieldPanel('theme'),
        SnippetChooserPanel('theme'),
    ]


class RoomIndexPage(WtBaseIndexPage):
    subpage_types = []
    body = RichTextField(blank=True)

    inline_template = 'wtrooms/includes/rooms.html'

    def render_details_theme(self):
        res = ''
        for item in self.room_items.all():
            try:
                res += self.render_item_theme(item)
            except AttributeError:
                pass

        return res;

    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('body', classname="full "),
        ],heading=_('Basic')),
        MultiFieldPanel([InlinePanel('room_items'),],heading=_("Rooms"),classname="collapsible collapsed"),
        MultiFieldPanel([InlinePanel('carousel_items'),],heading=_('Images'), classname="collapsible collapsed"),
        #MultiFieldPanel([InlinePanel('related_links'),],heading=_("Related links"),classname="collapsible collapsed"),
    ]


