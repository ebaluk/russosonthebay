# -*- coding: utf-8 -*-
import datetime
from django.db import models
from django import forms

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel


from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtpages.models import CarouselItem, RelatedLink
from wtblocks.models import ContentBlockThemed
from wtpages.models import *

from wtrooms2.models import *
from wtevents.models import *


class PromoBoxesPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('PromoBoxesPage', related_name='carousel_items')
    
class PromoBoxesPageSideCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('PromoBoxesPage', related_name='side_carousel_items')
    
class PromoBoxesPageBottomCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('PromoBoxesPage', related_name='bottom_carousel_items')    
        
class PromoBoxesPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('PromoBoxesPage', related_name='related_links')
    
class PromoBoxesPageFormLink(Orderable, WtFormLinkField):
    page = ParentalKey('PromoBoxesPage', related_name='form_links')

class PromoBoxesPageBoxItem(Orderable, CarouselItem):
    page = ParentalKey('PromoBoxesPage', related_name='box_items')
    title = models.CharField(max_length=255, verbose_name=_("Title 2"), blank=True, null=True)
    panels = [
        ImageChooserPanel('image'),
        #FieldPanel('embed_url'),
        FieldPanel('caption'),
        FieldPanel('text'),
        FieldPanel('title'),        
        MultiFieldPanel(LinkFields.panels, "Link"),
        FieldPanel('visible'),
    ]

PB_IMAGE_STYLE_CHOICES = (
    ('horizontal', _('Horizontal')),
    ('vertical', _('Vertical')),
    ('square', _('Square')),
)

PB_DATA_SOURCE_CHOICES = (
    ('custom', _('Custom')),
    ('rooms', _('Rooms')),
    ('events', _('Events')),    
)

class PromoBoxesPage(WtBasePage ):
    class Meta:
        verbose_name = _("Boxes Page")
    
    subpage_types = []    
    
    template = 'wtpromoboxes/promo_page.html'
    inline_template = 'wtpromoboxes/promo_page_inline.html'

    box_data_source = models.CharField(max_length=255, default='horizontal', choices=PB_DATA_SOURCE_CHOICES, )
    
    box_image_style = models.CharField(max_length=255, default='custom', choices=PB_IMAGE_STYLE_CHOICES, )
    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The outside page heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    body = RichTextField(blank=True, verbose_name = _("Text"))
    
    #hide_title = models.BooleanField(default=False)
    #show_index_title = models.BooleanField(default=False)    
    
    carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True, null=True)
    side_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    bottom_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    
    box_show_details_on_hover = models.BooleanField(default=False)
    box_show_title = models.BooleanField(default=False)
    box_columns = models.IntegerField(default=3)
    box_rows = models.IntegerField(default=3)
    
    box_min_width = models.IntegerField(default=250)
    box_max_width = models.IntegerField(default=350)
    
    
    
    def get_sitemap_urls(self):        
        return [] 
    
    @property
    def navbar_items(self):
        return self.get_parent().get_children().live().in_menu()
    
    @property
    def top_page_title(self):        
        return self.get_parent().title
    
    @property
    def top_page(self):
        return self.get_parent()
    
    
    @property
    def boxes(self):
        ret = None        
        if self.box_data_source == 'custom':
            ret = self.box_items.filter(visible=True)
        elif self.box_data_source == 'rooms':
            ret = []
            rooms = RoomType2Page.objects.live()
            for room in rooms:
                room.image = room.thumbnail
                room.caption = room.title
                room.title2 = room.page_content_heading                
                room.hide_title = True
                room.is_page = True                
                ret.append(room)
        elif self.box_data_source == 'events':
            ret = []
            rooms = EventPage.objects.live()
            for room in rooms:
                room.image = room.thumbnail
                room.caption = room.title
                room.hide_title = True
                room.is_page = True                
                ret.append(room)        
                
                 
        return ret        
             
        
        #return self.box_items.filter(visible=True)   
        
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('page_heading', classname="full title"),
            FieldPanel('page_content_heading', classname="full title"),            
            FieldPanel('body'),            
            
            
            
            FieldPanel('box_data_source'),
            FieldPanel('box_image_style'),
            FieldPanel('box_show_details_on_hover'),
            FieldPanel('box_show_title'),
            
            FieldPanel('box_rows'),
            FieldPanel('box_columns'),
            
            FieldPanel('box_min_width'),
            FieldPanel('box_max_width'),
            
            
            
            #FieldPanel('show_index_title'),
            #FieldPanel('hide_title'),
            SnippetChooserPanel('theme'),
        ],heading=_('Basic')),
                      
                      
        MultiFieldPanel([            
            InlinePanel('box_items'),],heading=_('Boxes'),classname="collapsible"),              
                      
        MultiFieldPanel([
            FieldPanel('carousel_style'),
            InlinePanel('carousel_items'),],heading=_('Top Images'),classname="collapsible collapsed"),
        MultiFieldPanel([
            FieldPanel('side_carousel_style'),
            InlinePanel('side_carousel_items'),],heading=_('Side Images'),classname="collapsible collapsed"),        
        MultiFieldPanel([
            FieldPanel('bottom_carousel_style'),
            InlinePanel('bottom_carousel_items'),],heading=_('Bottom Images'),classname="collapsible collapsed"),              
        MultiFieldPanel([InlinePanel('related_links'),], heading=_('Related Links'), classname="collapsible collapsed"),
        MultiFieldPanel([InlinePanel('form_links'),], heading=_('Forms'), classname="collapsible collapsed"),
    ]
    
    promote_panels = [
        MultiFieldPanel([            
            FieldPanel('slug'),
            FieldPanel('show_in_menus'),
            #FieldPanel('seo_title'),
            #FieldPanel('search_description'),
            #FieldPanel('seo_keywords'),
        ], _('Common page configuration')),
    ]


