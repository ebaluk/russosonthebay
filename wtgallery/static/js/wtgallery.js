$(function(){	
	var $carousel = $('section.slide-big.fullscreen-gallery .carousel');
	if( $carousel.length ){
		var $thumbnails = $('section.fullscreen-gallery-controls .iscroller.fullscreen-gallery-thumbnails');
		var timerId;
		
		$carousel.on('slide.bs.carousel', function (e) {
	    	var id = $(e.relatedTarget).data('id');
	    	$('.iscroller-item', $thumbnails).removeClass('active');
	    	var $iscrollerItem = $('.carousel-inner>.item.active .iscroller-item#thumbnail-'+id+':first', $thumbnails);
	    	if( ! $iscrollerItem.length ){
	    		$iscrollerItem = $('.iscroller-item#thumbnail-'+id+':first', $thumbnails);	
	    	}	    	
	    	$iscrollerItem.addClass('active');
	    	var activeIdx = $('.carousel-inner>.item', $thumbnails).index($iscrollerItem.closest(".item"));
	    	$('.carousel', $thumbnails).carousel(activeIdx);
	    });
	    
	    $($thumbnails).on('click', '.iscroller-item', function(e){
	    	e.preventDefault();
	    	var $this = $(this);	    	
	    	var id = $this.data('id');
	    	var activeIdx = $('.carousel-inner>.item', $carousel).index($('.carousel-inner>.item#img-'+id));
	    	$carousel.carousel(activeIdx);	    	
	    });
	    
	    $('section.fullscreen-gallery-controls  .collapse').on('show.bs.collapse', function(){
	    	clearTimeout(timerId);
	    	timerId = setTimeout(function(){
	    		$thumbnails.iscroller('update');          	
	         }, 10);	    	
	    });
	    
	    $thumbnails.on('filter', function(){
	    	var hash = {};
	    	$('.carousel-inner>.item', $carousel).remove();
	    	$('.iscroller-item', $thumbnails).removeClass('active');
	    	$('.iscroller-item:not(.hidden):first', $thumbnails).addClass('active');
	    	$('.iscroller-item:not(.hidden)', $thumbnails).each(function(idx){
	    		var $this = $(this);
	    		
	    		if( ! hash[$this.data('id')] ){
	    			hash[$this.data('id')] = 1; 
		    		var $item = $('<div/>').data('id', $this.data('id')).attr('id', 'img-'+$this.data('id')).addClass('item');
		    		if(!idx){
		    			$item.addClass('active');
		    		}
		    		$('<div/>').addClass('carousel-image').css({backgroundImage: 'url('+$this.data('image')+')'}).appendTo($item);
		    		$('<img/>').addClass('hidden').attr('src', $this.data('image')).attr('alt', $this.data('alt')).appendTo($item);
		    		$('.carousel-inner', $carousel).append($item);
	    		}
	    	});
	    	$('.carousel-inner .item:first', $carousel).addClass('active');
	    });
	}
});    