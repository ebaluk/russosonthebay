# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-08-30 04:04
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wtgallery', '0004_auto_20170830_0358'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gallerycollectionpagerelatedlink',
            name='link_document',
        ),
        migrations.RemoveField(
            model_name='gallerycollectionpagerelatedlink',
            name='link_page',
        ),
        migrations.RemoveField(
            model_name='gallerycollectionpagerelatedlink',
            name='page',
        ),
        migrations.AlterModelOptions(
            name='gallerycollectionpage',
            options={'verbose_name': 'Gallery Page (Collection Sets)'},
        ),
        migrations.DeleteModel(
            name='GalleryCollectionPageRelatedLink',
        ),
    ]
