# -*- coding: utf-8 -*-
from django.db import models
from django import forms

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel

from modelcluster.models import ClusterableModel


from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from wagtail.wagtailimages.models import *

from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel  #, get_all_child_relations

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtpages.models import WtBasePage, CarouselItem, RelatedLink
from wtblocks.models import ContentBlockThemed

from wtcollectionset import *


# class GalleryPageCarouselItem(Orderable, ClusterableModel): #models.Model,
#     page = ParentalKey('wtgallery.GalleryPage', related_name='gallery_items')
#     #tags = ClusterTaggableManager(through=GalleryPageItemTag, blank=True)
#     image = models.ForeignKey(
#         'wagtailimages.Image',
#         null=True,
#         blank=True,
#         on_delete=models.SET_NULL,
#         related_name='+'
#     )
#     caption = models.TextField(blank=True)
# 
#     panels = [
#         ImageChooserPanel('image'),
#         #FieldPanel('tags'),
#         FieldPanel('caption'),
#     ]
# 
# class GalleryPageTag(TaggedItemBase):
#    content_object = ParentalKey('wtgallery.GalleryPage', related_name='tagged_items')
# class GalleryPageRelatedLink(Orderable, RelatedLink):
#     page = ParentalKey('wtgallery.GalleryPage', related_name='related_links')
# 
# class GalleryPage(WtBasePage):
#     #body = RichTextField(blank=True, verbose_name=_("Text"))
#     tags = ClusterTaggableManager(through=GalleryPageTag, blank=True, verbose_name=_("Visible Categories Name"))
# 
#     subpage_types = []
#     inline_template = 'wtgallery/includes/gallery.html'
#     
#     content_panels = [
#         MultiFieldPanel([
#                 FieldPanel('title', classname="full title"),
#                 #FieldPanel('body', classname="full title"),
#             ],heading=_("Basic"),
#         ),
#         FieldPanel('tags'),
#         MultiFieldPanel([InlinePanel('gallery_items'), ],heading=_("Gallery Images"),classname="collapsible collapsed" ),
#         MultiFieldPanel([InlinePanel('related_links'), ],heading=_("Related links"),classname="collapsible collapsed" ),
#     ]
# 
#     class Meta:
#         verbose_name = _("Gallery Page")
        

class GalleryCollectionlItem(Orderable, ClusterableModel): #models.Model,
    page = ParentalKey('wtgallery.GalleryCollectionPage', related_name='collectionset_items')
    collection_set = models.ForeignKey(
        'wtcollectionset.CollectionSet',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    title = models.TextField(blank=True,)
    panels = [
        FieldPanel('title'),
        FieldPanel('collection_set'),
    ]


GALLERY_TYPE_CHOICES = (
    ('thumbnails', _('Thumbnails')),
    ('fullscreen', _('Full Screen')),
)

class GalleryCollectionPageCarouselItem(Orderable):
    page = ParentalKey('GalleryCollectionPage', related_name='carousel_items')    
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )   
    
    visible = models.BooleanField(blank=True, default=1)
    
    panels = [
        ImageChooserPanel('image'),
        FieldPanel('visible'),
    ]

class GalleryCollectionPage(WtBasePage):
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    body = RichTextField(blank=True, verbose_name=_("Text"))
    show_gallery_categories = models.BooleanField(default=True)    
    gallery_type = models.CharField(max_length=255, default='thumbnails', choices=GALLERY_TYPE_CHOICES, blank=True)
    subpage_types = []
    
    template = 'wtgallery/gallery_page.html'
    inline_template = 'wtgallery/gallery_page_inline.html'
    
    content_panels = [
        MultiFieldPanel([
                FieldPanel('title', classname="full title"),
                FieldPanel('page_heading', classname="full title"),
                FieldPanel('page_content_heading', classname="full title"),                
                FieldPanel('show_gallery_categories'),
                FieldPanel('gallery_type'),                
            ],heading=_("Basic"),
        ),
        MultiFieldPanel([InlinePanel('carousel_items'), ],heading=_("Top Images"),classname="collapsible collapsed" ),
        MultiFieldPanel([InlinePanel('collectionset_items'), ],heading=_("Gallery Categories"),classname="collapsible" ),        
    ]

    class Meta:
        verbose_name = _("Gallery Page (Collection Sets)")
        
    @property    
    def gallery_categories(self):                
        ret = []
        for cset in self.collectionset_items.all():
            cset.images = Image.objects.filter(collection_id__in=cset.collection_set.collection_items.values('collection__pk'))           
            ret.append(cset)
        return ret    
                

