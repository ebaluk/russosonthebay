from django.db import models
from django.utils.translation import ugettext_lazy as _
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailcore.models import Orderable
from modelcluster.fields import ParentalKey 

from wtpages.models import *

   

class InstagramThumbnailsPage(WtBasePage):
    class Meta:
        verbose_name = _('Instagram Thumbnails Page')
    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")        
    body = RichTextField(blank=True, help_text="Description")
    subpage_types = []
    template = 'wtinstalramthumbnails/instalram_thumbnails_page.html'
    inline_template = 'wtinstalramthumbnails/instalram_thumbnails_page_inline.html'
    
    page_latout = 'whole'
        
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title", ),    
            FieldPanel('page_heading', classname="full title", ),        
            FieldPanel('body'),                
        ],heading=_('Basic')),
                              
    ]
    

