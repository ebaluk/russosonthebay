# -*- coding: utf-8 -*-
from django.db import models
from django import forms

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel


from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtpages.models import CarouselItem, RelatedLink
from wtblocks.models import ContentBlockThemed
from wtpages.models import *



class MenuIndexPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('wtmenus.MenuIndexPage', related_name='carousel_items')
class MenuIndexPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('wtmenus.MenuIndexPage', related_name='related_links')

class MenuIndexPage(WtBaseIndexPage):
    subpage_types = ['wtmenus.MenuPage']
    body = RichTextField(blank=True)

    template = 'wtpages/standard_index_page.html'      

    @property
    def menus(self):
        menus = MenuPage.objects.live().descendant_of(self)
        return menus

    @property
    def navbar_items(self):
        return self.get_children().live()
    
    @property
    def top_page(self):
        return self

    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('body', classname="full "),
        ],heading=_('Basic')),
        MultiFieldPanel([InlinePanel('carousel_items'),],heading=_('Images'), classname="collapsible collapsed"),
        MultiFieldPanel([InlinePanel('related_links'),],heading=_("Related links"),classname="collapsible collapsed"),
    ]

    




class MenuPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('wtmenus.MenuPage', related_name='carousel_items')
class MenuPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('wtmenus.MenuPage', related_name='related_links')
class MenuPage(WtBasePage):
    body = models.TextField(blank=True)

    parent_page_types = ['wtmenus.MenuIndexPage']
    subpage_types = ['wtmenus.MenuCategoryPage']
    
    @property
    def menus_index(self):
        return MenuIndexPage.objects.get(id=self.get_parent().id)

    @property
    def menu_cats(self):
        menu_cats = MenuCategoryPage.objects.live().descendant_of(self)
        return menu_cats
    
    @property
    def navbar_items(self):
        return self.get_parent().get_children().live()
    
    @property
    def top_page(self):
        return self

    def __unicode__(self):
        return self.text


    def get_sitemap_urls(self):            
        return [
                    {
                        'location': self.specific.full_url,
                        'lastmod': self.latest_revision_created_at
                    }
                ]

    content_panels = [
        MultiFieldPanel(
            [
                FieldPanel('title', classname="full title"),
                FieldPanel('body', ),
            ],
            heading="Basic",
        ),
        MultiFieldPanel([InlinePanel('carousel_items'),],heading=_('Images'), classname="collapsible collapsed"),
        MultiFieldPanel([InlinePanel('related_links'),],heading=_("Related links"),classname="collapsible collapsed"),
    ]





class MenuItem(Orderable, models.Model):
    page = ParentalKey('wtmenus.MenuCategoryPage', related_name='menu_items')
    title = models.CharField(max_length=255)
    featured = models.BooleanField(default=False)
    body = RichTextField(blank=True, verbose_name=_('Description'))
    #price = models.DecimalField(max_digits=5, decimal_places=2, blank=True)
    price = models.CharField(max_length=255, blank=True)

class MenuCategoryPage(WtBasePage):
    body = RichTextField(blank=True)
    two_columns = models.BooleanField(default=False)
    featured = models.BooleanField(default=False)

    parent_page_types = ['wtmenus.MenuPage']
    subpage_types = []

    def __unicode__(self):
        return self.text

    def get_sitemap_urls(self):            
        return []

    content_panels = [
        MultiFieldPanel(
            [
                FieldPanel('title', classname="full title"),
                FieldPanel('body', ),
                FieldPanel('two_columns', ),
                FieldPanel('featured', ),
            ],
            heading="Basic",
        ),
        MultiFieldPanel([InlinePanel('menu_items'),],heading=_('Live Menu'), classname="collapsible collapsed"),
    ]
