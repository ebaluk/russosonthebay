from wagtail.wagtailcore.models import Collection
from wagtail.wagtailimages.models import *

def collection_images(collection_items):
    return Image.objects.filter(collection_id__in=collection_items.values('collection_id'))
    
    