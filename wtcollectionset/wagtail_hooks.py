from wagtail.contrib.modeladmin.options import ModelAdmin,  modeladmin_register

from .models import CollectionSet

class CollectionSetAdmin(ModelAdmin):
    model = CollectionSet
    menu_label = 'Collection Sets'
    menu_icon = 'folder-open-1'
    list_display = ('title',  )
    search_fields = ('title')
    menu_order = 750
    add_to_settings_menu = True

modeladmin_register(CollectionSetAdmin)
