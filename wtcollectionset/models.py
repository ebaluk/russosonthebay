from django.db import models
from django.utils.translation import ugettext_lazy as _

from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel

from wagtail.wagtailcore.models import Orderable
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel


class CollectionSetCollectionItem(Orderable):
    page = ParentalKey('CollectionSet', related_name='collection_items')
    collection = models.ForeignKey(
        'wagtailcore.Collection',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='+',
        help_text=_("The Collection to take Images and Documents from"),        
    )

class CollectionSet(ClusterableModel):
    title = models.CharField(max_length=255)
    panels = [
        MultiFieldPanel([FieldPanel('title', classname="full title"),],heading="Basic",),
        MultiFieldPanel([InlinePanel('collection_items'),],heading=_('Collections'),classname="collapsible "),
    ]
    
    def __str__(self):
        return self.title
    
    