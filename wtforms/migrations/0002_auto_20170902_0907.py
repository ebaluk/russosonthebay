# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-02 09:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wtpages', '0003_auto_20170829_1105'),
        ('wtforms', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='wtform',
            name='theme',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wtpages.CssTheme', verbose_name='Theme'),
        ),
        migrations.AddField(
            model_name='wtformfield',
            name='add_css_class',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Add CSS class'),
        ),
        migrations.AddField(
            model_name='wtformfield',
            name='placeholder',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Placeholder'),
        ),
        migrations.AddField(
            model_name='wtformfield',
            name='show_label',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='wtformfield',
            name='choices',
            field=models.TextField(blank=True, help_text='New line separated list of choices. Only applicable in checkboxes, radio and dropdown.', verbose_name='choices'),
        ),
        migrations.AlterField(
            model_name='wtformfield',
            name='default_value',
            field=models.CharField(blank=True, help_text='Default value. New line separated values supported for checkboxes.', max_length=255, verbose_name='default value'),
        ),
        migrations.AlterField(
            model_name='wtformfield',
            name='field_type',
            field=models.CharField(choices=[('singleline', 'Single line text'), ('multiline', 'Multi-line text'), ('email', 'Email'), ('number', 'Number'), ('url', 'URL'), ('checkbox', 'Checkbox'), ('checkboxes', 'Checkboxes'), ('dropdown', 'Drop down'), ('radio', 'Radio buttons'), ('date', 'Date'), ('datetime', 'Date/time'), ('captcha', 'Captcha'), ('file', 'File'), ('fieldsgroup', 'Fields Group'), ('stateselect', 'US State Select'), ('countryselect', 'Country Select')], max_length=16, verbose_name='field type'),
        ),
    ]
