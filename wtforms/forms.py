from __future__ import absolute_import, unicode_literals

from collections import OrderedDict

import django.forms
from django.utils.translation import ugettext_lazy as _

from wagtail.wagtailadmin.forms import WagtailAdminPageForm

from captcha.fields import CaptchaField
from .fields import WtFileInput, WtFileField, WtGroupField


US_STATES_CHOICES = (
        ('AK', _('Alaska')),
        ('AL', _('Alabama')),
        ('AR', _('Arkansas')),
        ('AS', _('American Samoa')),
        ('AZ', _('Arizona')),
        ('CA', _('California')),
        ('CO', _('Colorado')),
        ('CT', _('Connecticut')),
        ('DC', _('District of Columbia')),
        ('DE', _('Delaware')),
        ('FL', _('Florida')),
        ('GA', _('Georgia')),
        ('GU', _('Guam')),
        ('HI', _('Hawaii')),
        ('IA', _('Iowa')),
        ('ID', _('Idaho')),
        ('IL', _('Illinois')),
        ('IN', _('Indiana')),
        ('KS', _('Kansas')),
        ('KY', _('Kentucky')),
        ('LA', _('Louisiana')),
        ('MA', _('Massachusetts')),
        ('MD', _('Maryland')),
        ('ME', _('Maine')),
        ('MI', _('Michigan')),
        ('MN', _('Minnesota')),
        ('MO', _('Missouri')),
        ('MP', _('Northern Mariana Islands')),
        ('MS', _('Mississippi')),
        ('MT', _('Montana')),
        ('NA', _('National')),
        ('NC', _('North Carolina')),
        ('ND', _('North Dakota')),
        ('NE', _('Nebraska')),
        ('NH', _('New Hampshire')),
        ('NJ', _('New Jersey')),
        ('NM', _('New Mexico')),
        ('NV', _('Nevada')),
        ('NY', _('New York')),
        ('OH', _('Ohio')),
        ('OK', _('Oklahoma')),
        ('OR', _('Oregon')),
        ('PA', _('Pennsylvania')),
        ('PR', _('Puerto Rico')),
        ('RI', _('Rhode Island')),
        ('SC', _('South Carolina')),
        ('SD', _('South Dakota')),
        ('TN', _('Tennessee')),
        ('TX', _('Texas')),
        ('UT', _('Utah')),
        ('VA', _('Virginia')),
        ('VI', _('Virgin Islands')),
        ('VT', _('Vermont')),
        ('WA', _('Washington')),
        ('WI', _('Wisconsin')),
        ('WV', _('West Virginia')),
        ('WY', _('Wyoming')),
)


class WtBaseForm(django.forms.Form):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')

        self.user = kwargs.pop('user', None)
        self.page = kwargs.pop('page', None)

        super(WtBaseForm, self).__init__(*args, **kwargs)


class WtFormBuilder(object):
    def __init__(self, fields):
        self.fields = fields

    def create_singleline_field(self, field, options):
        # TODO: This is a default value - it may need to be changed
        options['max_length'] = 255
#         options['widget'] = django.forms.CharField.widget()
#         if field.placeholder:
#             options['widget'].attrs = {'placeholder': field.placeholder}             
        return django.forms.CharField(**options)

    def create_multiline_field(self, field, options):
        options['widget'] = django.forms.Textarea        
        return django.forms.CharField(**options)

    def create_date_field(self, field, options):        
        return django.forms.DateField(**options)

    def create_datetime_field(self, field, options):        
        return django.forms.DateTimeField(**options)

    def create_email_field(self, field, options):        
        return django.forms.EmailField(**options)

    def create_url_field(self, field, options):        
        return django.forms.URLField(**options)

    def create_number_field(self, field, options):
        return django.forms.DecimalField(**options)

    def create_dropdown_field(self, field, options):
        options['choices'] = map(
            lambda x: (x.strip(), x.strip()),
            field.choices.splitlines()
        )
        return django.forms.ChoiceField(**options)

    def create_radio_field(self, field, options):
        options['choices'] = map(
            lambda x: (x.strip(), x.strip()),
            field.choices.splitlines()
        )
        return django.forms.ChoiceField(widget=django.forms.RadioSelect, **options)

    def create_checkboxes_field(self, field, options):
        options['choices'] = [(x.strip(), x.strip()) for x in field.choices.splitlines()]
        options['initial'] = [x.strip() for x in field.default_value.splitlines()]
        return django.forms.MultipleChoiceField(
            widget=django.forms.CheckboxSelectMultiple, **options
        )

    def create_checkbox_field(self, field, options):
        return django.forms.BooleanField(**options)
    
    def create_captcha_field(self, field, options):
        return CaptchaField(**options)
    
    def create_file_field(self, field, options):
        return WtFileField(**options)
    
    def create_fields_group(self, field, options):
        return WtGroupField(**options)
    
    def create_state_select_field(self, field, options):
        options['choices'] =  ([(x.strip(), x.strip()) for x in field.choices.splitlines()] if field.choices else [('', _('Please Select'))]) + list(US_STATES_CHOICES)
        return django.forms.ChoiceField(**options)
    
    def create_country_select_field(self, field, options):                  
        from django_countries import countries
        options['choices'] =  ([(x.strip(), x.strip()) for x in field.choices.splitlines()] if field.choices else [('', _('Please Select'))]) + [(key, name) for key, name in countries]
        return django.forms.ChoiceField(**options)
    

    FIELD_TYPES = {
        'singleline': create_singleline_field,
        'multiline': create_multiline_field,
        'date': create_date_field,
        'datetime': create_datetime_field,
        'email': create_email_field,
        'url': create_url_field,
        'number': create_number_field,
        'dropdown': create_dropdown_field,
        'radio': create_radio_field,
        'checkboxes': create_checkboxes_field,
        'checkbox': create_checkbox_field,
        'singleline': create_singleline_field,
        'captcha' : create_captcha_field,
        'file' : create_file_field,
        'fieldsgroup': create_fields_group,
        'stateselect': create_state_select_field,
        'countryselect': create_country_select_field,
    }

    @property
    def formfields(self):
        formfields = OrderedDict()
        
        num = 1

        for field in self.fields:
            options = self.get_field_options(field)

            if field.field_type in self.FIELD_TYPES:
                formfields[field.clean_name] = self.FIELD_TYPES[field.field_type](self, field, options)
                formfields[field.clean_name].show_label = field.show_label
                formfields[field.clean_name].pk = field.pk
                formfields[field.clean_name].add_css_class = field.add_css_class
                formfields[field.clean_name].num = num
                num += 1
                
                if field.placeholder:
                    formfields[field.clean_name].widget.attrs['placeholder'] = field.placeholder 
                                
            else:
                raise Exception("Unrecognised field type: " + field.field_type)

        return formfields

    def get_field_options(self, field):
        options = {}
        options['label'] = field.label
        options['help_text'] = field.help_text
        options['required'] = field.required
        options['initial'] = field.default_value
        #options['attrs'] = {'placeholder': field.placeholder}         
        #formfields[field.clean_name].placeholder = field.placeholder
                
        return options

    def get_form_class(self):
        return type(str('WagtailForm'), (WtBaseForm,), self.formfields)


class SelectDateForm(django.forms.Form):
    date_from = django.forms.DateTimeField(
        required=False,
        widget=django.forms.DateInput(attrs={'placeholder': 'Date from'})
    )
    date_to = django.forms.DateTimeField(
        required=False,
        widget=django.forms.DateInput(attrs={'placeholder': 'Date to'})
    )


class WagtailAdminWtFormPageForm(WagtailAdminPageForm):

    def clean(self):

        super(WagtailAdminWtFormPageForm, self).clean()

        # Check for dupe form field labels - fixes #585
        if 'form_fields' in self.formsets:
            _forms = self.formsets['form_fields'].forms
            for f in _forms:
                f.is_valid()

            for i, form in enumerate(_forms):
                if 'label' in form.changed_data:
                    label = form.cleaned_data.get('label')
                    for idx, ff in enumerate(_forms):
                        # Exclude self
                        if idx != i and label == ff.cleaned_data.get('label'):
                            form.add_error(
                                'label',
                                django.forms.ValidationError(_('There is another field with the label %s, please change one of them.' % label))
                            )
