from __future__ import absolute_import, unicode_literals

from django.apps import AppConfig


class WtFormsAppConfig(AppConfig):
    name = 'wtforms'
    label = 'wtforms'
    verbose_name = "Form"
