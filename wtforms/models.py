# -*- coding: utf-8 -*-
import json
import os
import re
import uuid

#from django.core.files.storage import default_storage
#from django.core.files.base import ContentFile
#from wagtail.wagtailcore import hooks

from django.contrib.contenttypes.models import ContentType

from django.db import models
#from django import forms
from django.conf import settings as system_settings 

from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from unidecode import unidecode

#from django.core.files.storage import FileSystemStorage

from wagtail.wagtailcore.models import Page, Orderable, Collection, get_page_models
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel 
from wagtail.wagtailsnippets.edit_handlers import SnippetChooserPanel


from wagtail.wagtaildocs.models import Document
from modelcluster.fields import ParentalKey





## WT
from wtadmin.models import WtSettings

#from django.core.mail import get_connection, EmailMessage
from django.core.mail.backends.smtp import EmailBackend

from django.core.serializers.json import DjangoJSONEncoder
from wagtail.wagtailadmin.utils import send_mail
#from django.utils.six import text_type
from django.shortcuts import render
from django.template.loader import render_to_string
from django.http import JsonResponse

#from django.forms import fields
from captcha.fields import CaptchaField


from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache

from .forms import WtFormBuilder, WagtailAdminWtFormPageForm

import random
#from captcha.conf import settings
#from django.core.urlresolvers import reverse
from six import text_type
from modelcluster.models import ClusterableModel
from django.utils.safestring import mark_safe


from .fields import WtFileField, WtGroupField



def random_digit_challenge():
    ret = u''
    for _ in range(4):
        ret += str(random.randint(0,9))
    return ret, ret

class DisableCSRF(object):
    def process_request(self, request):
        setattr(request, '_dont_enforce_csrf_checks', True)


COLLECTION_NAME = 'Form Submissions'

FORM_FIELD_CHOICES = (
    ('singleline', _('Single line text')),
    ('multiline', _('Multi-line text')),
    ('email', _('Email')),
    ('number', _('Number')),
    ('url', _('URL')),
    ('checkbox', _('Checkbox')),
    ('checkboxes', _('Checkboxes')),
    ('dropdown', _('Drop down')),
    ('radio', _('Radio buttons')),
    ('date', _('Date')),
    ('datetime', _('Date/time')),
    ('captcha', _('Captcha')),
    ('file', _('File')),
    ('fieldsgroup', _('Fields Group')),
    ('stateselect', _('US State Select')),
    ('countryselect', _('Country Select')),
    
    
)

class WtAbstractFormField(Orderable):
    """
    Database Fields required for building a Django Form field.
    """

    label = models.CharField(
        verbose_name=_('label'),
        max_length=255,
        help_text=_('The label of the form field')
    )
    show_label = models.BooleanField(default=True)
    field_type = models.CharField(verbose_name=_('field type'), max_length=16, choices=FORM_FIELD_CHOICES)
    required = models.BooleanField(verbose_name=_('required'), default=True)
    choices = models.TextField(
        verbose_name=_('choices'),
        blank=True,
        help_text=_('New line separated list of choices. Only applicable in checkboxes, radio and dropdown.')
    )
    default_value = models.CharField(
        verbose_name=_('default value'),
        max_length=255,
        blank=True,
        help_text=_('Default value. New line separated values supported for checkboxes.')
    )
    help_text = models.CharField(verbose_name=_('help text'), max_length=255, blank=True)
    
    add_css_class = models.CharField(verbose_name=_('Add CSS class'), max_length=255, blank=True, null=True)
    placeholder = models.CharField(verbose_name=_('Placeholder'), max_length=255, blank=True, null=True)

    @property
    def clean_name(self):
        # unidecode will return an ascii string while slugify wants a
        # unicode string on the other hand, slugify returns a safe-string
        # which will be converted to a normal str
        return str(slugify(text_type(unidecode(self.label))))

    panels = [
        FieldPanel('label'),
        FieldPanel('placeholder'),
        FieldPanel('show_label'),        
        FieldPanel('help_text'),
        FieldPanel('required'),
        FieldPanel('field_type', classname="formbuilder-type"),
        FieldPanel('choices', classname="formbuilder-choices"),
        FieldPanel('default_value', classname="formbuilder-default"),
        FieldPanel('add_css_class'),        
        
        
    ]

    class Meta:
        abstract = True
        ordering = ['sort_order']


class WtFormField(WtAbstractFormField):
    page = ParentalKey('WtForm', related_name='form_fields')


# class WtDocument(Document):    
#     def __init__(self, *args, **kwargs):
#         super(WtDocument, self).__init__(*args, **kwargs)
#         #self.file = models.FileField(upload_to=kwargs['upload_to'], verbose_name=_('file'))

from wagtail.wagtailforms.models import AbstractEmailForm
class FormPage(AbstractEmailForm):
    class Meta:
        verbose_name = "Form"
    parent_page_types = []
    subpage_types = []        


class WtForm(ClusterableModel):
    class Meta:
        verbose_name = "Form"
        
    base_form_class = WagtailAdminWtFormPageForm    
    form_builder = WtFormBuilder    


    theme = models.ForeignKey(
        'wtpages.CssTheme',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name = _("Theme")
    )    
    
    body = RichTextField(blank=True)
    thank_you_text = RichTextField(blank=True)
    ajax_form = True

    template_fields = 'wtforms/form_fields.html'
    template = 'wtforms/form.html'    
    
    name = models.CharField(max_length=255)
    title = models.CharField(blank=True, max_length=255)
    
    to_address = models.CharField(
        verbose_name=_('to address'), max_length=255, blank=True,
        help_text=_("Optional - form submissions will be emailed to these addresses. Separate multiple addresses by comma.")
    )
    from_address = models.CharField(verbose_name=_('from address'), max_length=255, blank=True)
    subject = models.CharField(verbose_name=_('subject'), max_length=255, blank=True)
    

    def __str__(self):
        return '%s' % self.name
    
    def get_context(self, request, *args, **kwargs):
        return {
            'page': self,
            'self': self,
            'request': request,
        }

    def get_template(self, request, *args, **kwargs):
        #if request.is_ajax():
        #    return self.ajax_template or self.template
        #else:
        return self.template
    
    def get_form_fields(self):
        return self.form_fields.all()

    def get_data_fields(self):
        data_fields = [
            ('submit_time', _('Submission date')),
        ]
        for field in self.get_form_fields():
            if field.field_type != 'fieldsgroup':
                data_fields += [(field.clean_name, field.label)]
                
#         data_fields += [
#             (field.clean_name, field.label) if field.field_type != 'fieldsgroup' else continue for field in self.get_form_fields()
#         ]
        return data_fields

    def get_form_class(self):
        fb = self.form_builder(self.get_form_fields())
        return fb.get_form_class()

    def get_form_parameters(self):
        return {}

    def get_form(self, *args, **kwargs):
        form_class = self.get_form_class()
        form_params = self.get_form_parameters()
        form_params.update(kwargs)

        return form_class(*args, **form_params)

    def get_submission_class(self):
        return WtFormSubmission
    
    def handle_files_upload(self, form, request):
        for filename, file in request.FILES.items():
            folder = str(uuid.uuid4())                                        
            dest_folder = os.path.join(system_settings.UPLOADS_ROOT, folder)
            
            fn, ex = os.path.splitext(file.name)            
            bn = os.path.basename(fn)
            
            dx = 54 - len(bn) - len(ex)
            if dx < 0:
                bn = bn[:dx]

            fn = "%s%s" % (bn, ex)     
                  
            #uploads/79d9ae0a-3671-4a6a-9408-7ce0485e1d25/
            dest = os.path.join(dest_folder, fn)
            save_upload(file, dest)    
             
            #logger.info('Upload saved: %s' % dest)
            
        
            cname =  '%s - %s' % (COLLECTION_NAME, self.name )        
            
            try:
                
                try:
                    theCollection = Collection.objects.get(name=cname)
                except Collection.DoesNotExist:
                    theCollection = Collection.get_first_root_node().add_child(instance = Collection( name=cname ))
                    theCollection.save()
                      
                #file=models.FileField(upload_to="uploads/%s" % folder, verbose_name=_('file'))                        
                doc = Document(title=file.name)
                #doc.file.upload_to("uploads/%s" % folder)
                doc.file = os.path.join("uploads", folder, fn)               
                doc.collection_id = theCollection.id
                doc.save()
                
                form.cleaned_data[re.sub(r'_0$', '', filename)] = request.build_absolute_uri( doc.url )
                
            except:
                pass     
    
    def process_form_submission(self, form):
        email_back = ''
#         for x in form.fields.items():
#             if isinstance(x[1], fields.EmailField):
#                 text_type(form.data.get(x[0]))
#                 email_back = text_type(form.data.get(x[0]))
#                 break

        #####super(AbstractEmailForm, self).process_form_submission(form)
        urls = re.compile(r"((https?):((//)|(\\\\))+[\w\d:#@%/;$()~_?\+-=\\\.&]*)", re.MULTILINE|re.UNICODE)
        data = {}
        for name, field in form.fields.items():
            bf = form[name]
            val = form.cleaned_data[name]            
                
            if isinstance(field, WtFileField):                    
                val = mark_safe( urls.sub(r'<a href="\1" target="_blank">'+os.path.basename(text_type(val))+'</a>', val) )
                
            if not isinstance(field, CaptchaField) and not isinstance(field, WtGroupField):
                data[name] = val
        
        
        
        WtFormSubmission.objects.create(
            form_data=json.dumps(data, cls=DjangoJSONEncoder),                        
            page=self,
        )

        config = WtSettings.objects.all()[0]
        if self.to_address:
            
            data = []
            
            for name, field in form.fields.items():
                bf = form[name]
                val = text_type(form.cleaned_data[name])
                #val = text_type(form.data.get(x[0]))
                
                if isinstance(field, WtFileField):                    
                    val = mark_safe( urls.sub(r'<a href="\1" target="_blank">'+os.path.basename(val)+'</a>', val) )
                
                if not isinstance(field, CaptchaField) and not isinstance(field, WtGroupField):
                    data.append({'label': text_type(bf.label), 'value': val})
            
            html = render_to_string("wtforms/mail.html", {'form': data})
            
#             from django.core.files import File    
#             f = open('/tmp/mail.html', 'w')
#             myfile = File(f)
#             myfile.write(html)
#             myfile.close()
            
             
            content = '\n'.join([x[1].label + ': ' + text_type(form.data.get(x[0])) for x in form.fields.items()])
            backend = None
            if config.smtp_host:
                backend = EmailBackend(
                    host=config.smtp_host,
                    username=config.smtp_user,
                    password=config.smtp_pass,
                )
            for adr in self.to_address.split(','):
                adr = adr.split()
                send_mail(self.subject, content, adr, self.from_address, connection=backend, html_message=html )  #, fail_silently=False,

            if email_back:
                send_mail(self.subject, content, [email_back], self.from_address, connection=backend, html_message=html )  #, fail_silently=False,        
            
        
    def send_mail(self, form):
        addresses = [x.strip() for x in self.to_address.split(',')]
        content = '\n'.join([x[1].label + ': ' + text_type(form.data.get(x[0])) for x in form.fields.items()])
        send_mail(self.subject, content, addresses, self.from_address,)
    
    @csrf_exempt
    @never_cache
    def serve(self, request):
        
        
        context = self.get_context(request)
              
        if request.method == 'POST':
            #print(request.COOKIES)
            #print(request.META['HTTP_X_CSRFTOKEN'])
            
            form = self.get_form(request.POST, request.FILES)            
                        
            if self.ajax_form:
                cont = self.get_context(request)
                if form.is_valid():
                    
                    self.handle_files_upload(form, request)
                    
                    self.process_form_submission(form)
                    cont['form'] = form
                    res = {
                        'status': 'success',
                        'message_text': self.thank_you_text,
                        'message_title': self.title,
                    }
                else:
                    cont['form'] = form
                    res = {
                        'status': 'error',
                        'form_html': render_to_string(self.template_fields, cont),                        
                    }
                return JsonResponse(res)

            elif form.is_valid():
                
                    self.handle_files_upload(form, request)
                
                    self.process_form_submission(form)
                    # render the landing_page
                    # TODO: It is much better to redirect to it
                    return render(
                        request,
                        self.landing_page_template,
                        self.get_context(request)
                    )

        else:
            form = self.get_form()
            context['is_popup'] = True
            context['modal_only'] = True
            

        
        context['form'] = form        
        return render(
            request,
            self.template,
            context
        )

    @property
    def submissions_size(self):
        return self.get_submission_class().objects.filter(page=self).count()
            

    panels = [
        MultiFieldPanel([
            FieldPanel('name', classname="full title"),
            FieldPanel('title', classname="full title"),
            FieldPanel('body'),
            FieldPanel('thank_you_text', classname="full"),
            SnippetChooserPanel('theme'),            
        ], "Basic"),

        MultiFieldPanel([InlinePanel('form_fields',),],heading='Form fields',classname="collapsible "),
        

        MultiFieldPanel([
            FieldPanel('to_address', classname="full"),
            FieldPanel('from_address', classname="full"),
            FieldPanel('subject', classname="full"),
        ], "Email" ,classname="collapsible collapsed"),

    ]



class WtFormSubmission(models.Model):
    
    form_data = models.TextField()
    page = models.ForeignKey(WtForm, on_delete=models.CASCADE)

    submit_time = models.DateTimeField(verbose_name=_('submit time'), auto_now_add=True)

    def get_data(self):
        """
        Returns dict with form data.

        You can override this method to add additional data.
        """
        form_data = json.loads(self.form_data)
        form_data.update({
            'submit_time': self.submit_time,
        })

        return form_data

    def __str__(self):
        return self.form_data

    class Meta:
        verbose_name = _('Wt Form submission')

_FORM_CONTENT_TYPES = None


def get_form_types():
    global _FORM_CONTENT_TYPES
    if _FORM_CONTENT_TYPES is None:
        form_models = [
            model for model in get_page_models()
            if issubclass(model, WtForm)
        ]

        _FORM_CONTENT_TYPES = list(
            ContentType.objects.get_for_models(*form_models).values()
        )
    return _FORM_CONTENT_TYPES
     
     
def get_forms_for_user(user):
    """
    Return a queryset of form pages that this user is allowed to access the submissions for
    """
#     editable_forms = UserPagePermissionsProxy(user).editable_pages()
#     editable_forms = editable_forms.filter(content_type__in=get_form_types())
# 
#     # Apply hooks
#     for fn in hooks.get_hooks('filter_form_submissions_for_user'):
#         editable_forms = fn(user, editable_forms)
# 
#     return editable_forms

    return WtForm.objects.all()
    
    
class WtFormLinkField(models.Model):
    is_popup = models.BooleanField("Popup Form", blank=True, default=False)
    form = models.ForeignKey(
        'wtforms.WtForm',
        null=False,
        blank=False,
        related_name='+',
    )

    panels = [
        FieldPanel('form'),
        FieldPanel('is_popup'),
    ]

    class Meta:
        abstract = True
    
    
class FormLinkPage(Page):
    class Meta:
        verbose_name = _("Form Popup Page")
         
    form = models.ForeignKey(
        'wtforms.WtForm',
        null=True,
        blank=True,
        related_name='+',
        on_delete=models.SET_NULL        
    )
    #external = models.BooleanField(default=False, help_text='Open link in a new window')
 
 
    def get_sitemap_urls(self):
        return []
 
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="title"),
            FieldPanel('form'),
            #FieldPanel('external'),
        ],heading=_('Basic')),
    ]
    promote_panels = [
        MultiFieldPanel([
            FieldPanel('show_in_menus'),
            FieldPanel('slug'),
            #FieldPanel('seo_title'),
            #FieldPanel('search_description'),
            #FieldPanel('seo_keywords'),
        ], _('Common page configuration')),
    ]

    
    
    
def save_upload(f, path):    
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(os.path.dirname(path))
    with open(path, 'wb+') as destination:
        if hasattr(f, 'multiple_chunks') and f.multiple_chunks():
            for chunk in f.chunks():
                destination.write(chunk)
        else:
            destination.write(f.read())


        