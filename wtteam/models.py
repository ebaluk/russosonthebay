# -*- coding: utf-8 -*-
from django.db import models
from django import forms

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel


from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtpages.models import CarouselItem, RelatedLink
from wtblocks.models import ContentBlockThemed
from wtpages.models import *
import re


@register_snippet
class TeamPosition(Orderable):
    class Meta:
        verbose_name = _('Team Position')
        verbose_name_plural=_('Team Positions')
    title = models.CharField(max_length=255)

    def __str__(self):
        return self.title

    panels = [
        MultiFieldPanel([
                FieldPanel('title', classname=""),
            ],
            heading=_("Basic"),
        ),
    ]




class TeamIndexPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('wtteam.TeamIndexPage', related_name='carousel_items')
#class TeamIndexPageRelatedLink(Orderable, RelatedLink):
    #page = ParentalKey('wtteam.TeamIndexPage', related_name='related_links')
class TeamItem(Orderable, models.Model):
    page = ParentalKey('wtteam.TeamIndexPage', related_name='team_items')
    title = models.CharField(max_length=255)
    position = models.ForeignKey(
        'wtteam.TeamPosition',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name = _("Position")
    )
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name = _("Image")
    )
    body = RichTextField(blank=True, verbose_name=_('Description'))
    theme = models.ForeignKey(
        'wtpages.CssTheme',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name = _("Theme")
    )
    panels = [
        FieldPanel('title'),
        FieldPanel('position'),
        ImageChooserPanel('image'),
        FieldPanel('body'),
        FieldPanel('theme'),
    ]


class TeamIndexPage(WtBaseIndexPage):
    subpage_types = []
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    body = RichTextField(blank=True)
    show_details = models.BooleanField(default=False)
    page_layout = 'whole'

    template = 'wtteam/team_page.html'
    inline_template = 'wtteam/team_page_inline.html'

    def render_details_theme(self):
        res = ''
        for item in self.team_items.all():
            try:
                res += self.render_item_theme(item)
            except AttributeError:
                pass

        return res;

    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('page_heading', classname="full title"),
            FieldPanel('page_content_heading', classname="full title"),            
            FieldPanel('body', classname="full "),
            FieldPanel('show_details', classname="full "),
            
        ],heading=_('Basic')),
        MultiFieldPanel([InlinePanel('team_items'),],heading=_("Team members"),classname="collapsible collapsed"),
        MultiFieldPanel([InlinePanel('carousel_items'),],heading=_('Images'), classname="collapsible collapsed"),
        #MultiFieldPanel([InlinePanel('related_links'),],heading=_("Related links"),classname="collapsible collapsed"),
    ]


