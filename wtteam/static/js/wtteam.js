
$(function(){		
	$('section.teamindexpage .team-thumbnails').on('click', '.list-item a', function(e){
		e.preventDefault();
		var $this = $(this);
		var id = $this.data('id');
		$carousel = $('.carousel.team-details');
		$items = $('.carousel-inner>.item', $carousel);		
		var idx = $items.index($('.carousel-inner>.item.item-'+id, $carousel));
		//console.log(idx, $('.carousel-inner>.item.item-'+id, $carousel));
		$carousel.carousel(idx);
		$carousel.show(800);
	});	
});
