# -*- coding: utf-8 -*-
from django.db import models
from wagtail.wagtailcore import blocks
from wagtail.wagtailimages.blocks import ImageChooserBlock

#locale
from django.utils.translation import ugettext_lazy as _


class ImagesSet(blocks.StructBlock):
    image = ImageChooserBlock(required=True, label=_('Image'))
    header = blocks.CharBlock(required=False, label=_('Name'))
    description = blocks.TextBlock(required=False, label=_('Description'))
    url = blocks.URLBlock(blank=True,required=False, label=_('Link'))
    page = blocks.PageChooserBlock(blank=True,required=False, label=_('Page Link'))

    class Meta:
        icon='cogs'
        template='wtblocks/images_set.html'

IMAGE_THEMES_CHOICES = (
    ('window-width', _("Screen wide, stretched")),
    ('content-width', _("Content wide, stretched")),
    ('original-width-centered', _("Original, centered")),
)
TEXT_POS_CHOICES = (
    ('top-left',     _("Top left (50% width)")),
    ('top',          _("Top (100% width)")),
    ('top-right',    _("Top right (50% width)")),
    ('bottom-left',  _("Bottom left (50% width)")) ,
    ('bottom',       _("Bottom (100% width)")),
    ('bottom-right', _("Bottom right (50% width)")),
)
class ImageThemed(blocks.StructBlock):
    theme = blocks.ChoiceBlock(choices=IMAGE_THEMES_CHOICES,required=False, classname="full", label=_("Image style"))
    image = ImageChooserBlock(classname="full",required=False, label=_("Image"))
    text = blocks.RichTextBlock(classname="full",required=False, label=_("Text"))
    position = blocks.ChoiceBlock(choices=TEXT_POS_CHOICES,required=False, classname="full", label=_("Text position"))
    class Meta:
        template='wtblocks/image.html'

class ContentBlock(blocks.StreamBlock):
    h1 = blocks.CharBlock(classname="full", label=_("Header (h1)"), icon = 'title')
    h2 = blocks.CharBlock(classname="full", label=_("Header (h2)"), icon = 'title')
    h3 = blocks.CharBlock(classname="full", label=_("Header (h3)"), icon = 'title')
    text = blocks.RichTextBlock(classname="full", label=_("Text"), icon='pilcrow')
    image = ImageThemed(template='wtblocks/image.html', label=_('Image'), icon='image')
    images_set = blocks.ListBlock(ImagesSet(), template='wtblocks/images_set.html',label=_("Image block"), icon='cog')
    #page = blocks.PageChooserBlock(blank=True,required=False)
    class Meta:
        icon = 'user'
        label=_("Page content")



BLOCK_THEMES_CHOICES = (
    ('white-bg', _("White")),
    ('light-blue-bg', _("Lightblue")),

    #('gray-bg', "Серый бэкграунд"),
    #('dark-bg', "Темно-серый бэкграунд, белый текст"),
    #('red-bg', "Темно-красный бэкграунд, белый текст"),
    #('black-bg', "Черный бэкграунд, белый текст"),
    #('white-border-bg', "Белый бэкграунд, с серой полосой"),
)
class ContentBlockThemed(blocks.StructBlock):
    theme = blocks.ChoiceBlock(required=True, choices=BLOCK_THEMES_CHOICES, label=_("Theme"))
    block_content = ContentBlock()
    class Meta:
        icon = 'user'
        template = 'wtblocks/content_block.html'
        form_classname = 'content-block struct-block pages-content-block'
