# -*- coding: utf-8 -*-
from django.db import models
from django import forms

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel


from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtpages.models import CarouselItem, RelatedLink
from wtblocks.models import ContentBlockThemed
from wtpages.models import *

from django.utils.safestring import mark_safe
from django.utils.html import format_html

class Testimonial(models.Model):
    class Meta:
        verbose_name = _('Testimonial')
        verbose_name_plural = _('Testimonials')
        ordering = ['sort_order']
    
    author = models.CharField(max_length=255)
    html = RichTextField(blank=True)    
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )    
    sort_order = models.IntegerField(default=0)
    visible = models.BooleanField(blank=True, default=1)

    @property
    def text(self):
        #return self.html
        return format_html("""
        <div class="testimonial-text">{}</div>
        <div class="testimonial-author">-{}</div>
        """, 
                           mark_safe(self.html), 
                           self.author )
        
    panels = [
        MultiFieldPanel([
            FieldPanel('author'),
            FieldPanel('html' ),
            ImageChooserPanel('image'),
            FieldPanel('visible' ),
            FieldPanel('sort_order' ),                        
            #DocumentChooserPanel('link_document'),
        ],heading="Basic",),
                        
    ]
        
    def __str__(self):
        return self.author
    
class TestimonialItem(Testimonial, Orderable):
    page = ParentalKey('TestimonialsPage', related_name='testimonial_items')
    panels = [
        MultiFieldPanel([
            FieldPanel('author'),
            FieldPanel('html' ),
            ImageChooserPanel('image'),
            FieldPanel('visible' ),
            #FieldPanel('sort_order' ),                        
            #DocumentChooserPanel('link_document'),
        ],heading="Basic",),
    ]          
    
class TestimonialsPage(WtBasePage):
    class Meta:
        verbose_name = _('Testimonials Page')        
    subpage_types = []
    inline_template = 'wttestimonials/includes/testimonials.html'
    
    use_shared_testimonials = models.BooleanField(default=True)
    
    @property
    def testimonials(self):
        return Testimonial.objects.filter(visible=True) if self.use_shared_testimonials else self.testimonial_items.filter(visible=True) 
    
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('use_shared_testimonials'),                        
            FieldPanel('theme'),
        ],heading=_('Basic')),
        MultiFieldPanel([InlinePanel('testimonial_items'),],heading=_("Testimonials"),classname="collapsible collapsed"),        
    ]



# class TestimonialIndexPageCarouselItem(Orderable, CarouselItem):
#     page = ParentalKey('wttestimonials.TestimonialIndexPage', related_name='carousel_items')
# class TestimonialItem(Orderable, models.Model):
#     page = ParentalKey('wttestimonials.TestimonialIndexPage', related_name='testimonial_items')
#     title = models.CharField(max_length=255, verbose_name=_('Name'))
#     text = models.TextField(max_length=255, verbose_name=_('Kind words'), blank=True)
#     link = models.URLField(max_length=255, blank=True)
#     visible = models.BooleanField(default=True)
#     image = models.ForeignKey(
#         'wagtailimages.Image',
#         null=True,
#         blank=True,
#         on_delete=models.SET_NULL,
#         related_name='+',
#         verbose_name = _("Image")
#     )
#     #body = RichTextField(blank=True, verbose_name=_('Description'))
#     panels = [
#         FieldPanel('title'),
#         FieldPanel('text'),
#         #FieldPanel('link'),
#         ImageChooserPanel('image'),
#         FieldPanel('visible'),
#         #FieldPanel('position'),
#         #FieldPanel('body'),
#     ]
# 
# 
# class TestimonialIndexPage(WtBaseIndexPage):
#     subpage_types = []
#     body = RichTextField(blank=True)
#     inline_template = 'wttestimonials/includes/testimonials.html'
#     content_panels = [
#         MultiFieldPanel([
#             FieldPanel('title', classname="full title"),
#             FieldPanel('body', classname="full "),
#             FieldPanel('theme'),
#         ],heading=_('Basic')),
#         MultiFieldPanel([InlinePanel('testimonial_items'),],heading=_("Testimonials"),classname="collapsible collapsed"),
#         MultiFieldPanel([InlinePanel('carousel_items'),],heading=_('Images'), classname="collapsible collapsed"),
#         #MultiFieldPanel([InlinePanel('related_links'),],heading=_("Related links"),classname="collapsible collapsed"),
#     ]
