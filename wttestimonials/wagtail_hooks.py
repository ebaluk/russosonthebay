from wagtail.contrib.modeladmin.options import ModelAdmin,  modeladmin_register

from .models import Testimonial

class TestimonialAdmin(ModelAdmin):
    model = Testimonial
    menu_label = 'Testimonials'
    menu_icon = 'doc-full'
    list_display = ('author',  'sort_order')
    search_fields = ('author')
    menu_order = 431
    add_to_settings_menu = False

modeladmin_register(TestimonialAdmin)
