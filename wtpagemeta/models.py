from django.db import models
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel 

#locale
from django.utils.translation import ugettext_lazy as _

@register_setting
class WtPageMeta(BaseSetting):
    class Meta:
        verbose_name = _("Page Meta")
    site_name = models.CharField(max_length=255)
    default_title = models.CharField(max_length=255, blank=True, null=True )
    default_description = models.TextField(blank=True, null=True )
    default_keywords = models.TextField(blank=True, null=True )
    default_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    fb_app_id = models.CharField(max_length=255, blank=True, null=True, verbose_name = _("Facebook APP ID"))
    
    panels = [
        MultiFieldPanel([
                FieldPanel('site_name'),
                FieldPanel('default_title'),
                FieldPanel('default_description'),
                FieldPanel('default_keywords'),              
                FieldPanel('fb_app_id'),
                ImageChooserPanel('default_image'),
            ],
            heading=_('Common'),
        ),
    ]
