import re
import os
import datetime
import uuid

from django.template.loader import render_to_string
from datetime import date
from django import template
from django.conf import settings
from django.utils import translation
from django.utils.safestring import mark_safe

from wtpages.models import *
from wtcontacts.models import *
from wtsocialfeed.models import *

from django.conf import settings as system_settings
import logging
logger = logging.getLogger('django')

register = template.Library()


@register.simple_tag(takes_context=True)
def pageurl(context, page):        
    try:
        if page.get_parent().specific.subpages_inline:
            return '%s#%s' % ( page.get_parent().relative_url(context['request'].site), page.slug ) 
    except:
        pass           
    
    return page.relative_url(context['request'].site)


@register.simple_tag()
def debug_object_dump(var):
    try:
        return vars(var)
    except:
        return 'Something wrong in debug_object_dump'

@register.assignment_tag(takes_context=True)
def get_request_meta(context):
    return context['request'].META
    

@register.assignment_tag(takes_context=True)
def get_form_inline(context, form):
    ff = form.get_form()
    return {
        'form': ff,
        'request': context['request'],
    }

@register.simple_tag(takes_context=True)
def get_contact_page(context):
    return ContactPage.objects.live().filter(default_contact=True).first()              


@register.simple_tag
def get_all_static_files():
    ret = {'css': [], 'js': [],}
    for app in system_settings.INSTALLED_APPS:
        fl = os.path.join('js', '%s.js' %  app )  
        if os.path.exists(os.path.join(system_settings.BASE_DIR, app, "static", fl)):
            ret['js'].append(mark_safe(fl))            
            
        fl = os.path.join('css', '%s.css' %  app )  
        if os.path.exists(os.path.join(system_settings.BASE_DIR, app, "static", fl)):
            ret['css'].append({'t': mark_safe('text/css'), 'f':mark_safe(fl)})
            
        fl = os.path.join('css', '%s.scss' %  app )  
        if os.path.exists(os.path.join(system_settings.BASE_DIR, app, "static", fl)):
            ret['css'].append({'t': mark_safe('text/x-scss'), 'f':mark_safe(fl)})    
                
    return ret                 
       
@register.simple_tag
def get_settings():                    
    return settings                 

       

@register.assignment_tag(takes_context=True)
def get_site_root(context):
    rp = context['request'].site.root_page
    return rp
    #url = context['request'].path
    #language = url.split('/')[1:2][0]
    #return rp.get_children().live().filter(slug=language)[0]

@register.assignment_tag(takes_context=True)
def get_site_lang(context, self):
    return 'en'
    #language_homepage = self.get_ancestors(inclusive=True).get(depth=3)
    #translation.activate(language_homepage.slug)
    #return language_homepage.slug

@register.inclusion_tag('wtpages/tags/breadcrumbs.html', takes_context=True)
def breadcrumbs(context):
    self = context.get('self')
    if self is None or self.depth <= 2:
        ancestors = ()
    else:
        ancestors = Page.objects.ancestor_of(self, inclusive=True).filter(depth__gt=1)
    return {
        'ancestors': ancestors,
        'request': context['request'],
    }


def has_menu_children(page):
    return page.get_children().live().in_menu().exists()

@register.inclusion_tag('wtpages/tags/top_menu.html', takes_context=True)
def top_menu(context, parent, calling_page=None):
    
    menuitems = parent.get_children().live().in_menu()
    
    cnt = len(menuitems)
    for menuitem in menuitems:
        #if rp != parent:
        menuitem.show_dropdown = has_menu_children(menuitem)        
        menuitem.is_logo = False
        try:
            if menuitem.specific.hide_dropdown:
                menuitem.show_dropdown = False
        except AttributeError: pass
        try:
            if menuitem.specific.link:
                menuitem.link = menuitem.specific.link
                menuitem.external = menuitem.specific.external
        except AttributeError: pass

        
        menuitem.width = str(100/cnt).replace(',',','); 
        menuitem.active = (calling_page.url.startswith(menuitem.url)
                           if calling_page else False)
    
    rp = context['request'].site.root_page
    menuitems = list(menuitems)
    
    if 'logo-center' == settings.MAIN_MENU_TYPE:
        menuitems.insert(int(cnt/2 + 0.5), {'is_logo':True})
    #menuitems.insert(int(cnt/2), {'is_logo':True})
    #if 1 == cnt % 2:
    #    pass

    if parent.show_in_menus:
        menuitems.insert(0,parent)

    return {        
        'calling_page': calling_page,
        'request': context['request'],
        'menuitems': menuitems,
        'itemcount': cnt,
    }

@register.inclusion_tag('wtpages/tags/top_menu_children.html', takes_context=True)
def top_menu_children(context, parent, show_all=False):
    menuitems_children = None
    if parent:
        menuitems_children = parent.get_children()
        if menuitems_children:
            if show_all:
                menuitems_children = menuitems_children.live()
            else:
                menuitems_children = menuitems_children.live().in_menu()
    return {
        'parent': parent,
        'dropdown': not show_all,
        'menuitems_children': menuitems_children,
        'request': context['request'],
    }

@register.inclusion_tag('wtpages/tags/page_navbar.html', takes_context=True)
def page_navbar(context, page):
    menuitems = page.navbar_items
    cnt = len(menuitems)
    for menuitem in menuitems:
        menuitem.active = (page.url.startswith(menuitem.url) if page else False)
    return {
        'calling_page': page,
        'menuitems': menuitems,
        'itemcount': cnt,
        'request': context['request'],
    }

@register.inclusion_tag('wtcontacts/includes/address.html', takes_context=True)
def address(context):     
    contact = ContactPage.objects.live().filter(default_contact=True).first()
    context['contact'] = contact
    return {
        'contact': contact,
        'request': context['request'],
    }
    
    
@register.inclusion_tag('wtcontacts/includes/hours.html', takes_context=True)
def hours(context):
    contact = ContactPage.objects.live().filter(default_contact=True).first()
    context['contact'] = contact
    return {
        'contact': contact,
        'request': context['request'],
    }    

# @register.inclusion_tag('wtpages/includes/carousel.html', takes_context=True)
# def featured_events(context):
#     items = None
#     event_page = EventIndexPage.objects.live()
#     if event_page:
#         event_page = event_page[0]
#         items = event_page.get_items( datetime.datetime.today().replace(day=1) ).filter(featured=1)
# 
#         for item in items:
#             item.visible = True
#             item.caption = item.title
#             item.link = event_page.url
# 
#     return {
#         'carousel_style': 'fading',
#         'carousel_items': items,
#         'image_size': 'half',
#         'request': context['request'],
#     }
    
@register.inclusion_tag('wtpages/includes/carousel.html', takes_context=True)
def carousel(context, carousel_items, fallback_items=False, show_controls=False, carousel_style='fading', imagesize='', carousel_class='slideshow' ):
    items = carousel_items if carousel_items else fallback_items
    return {
        'id': 'carousel-' + str(uuid.uuid4()),
        'show_controls': show_controls,
        'carousel_style': carousel_style,
        'carousel_items': items,  
        'imagesize': imagesize,
        'carousel_class': carousel_class,
        'request': context['request'],
    }   

@register.assignment_tag(takes_context=True)
def get_reservation_times(context):
    #lst = []
    start = datetime.datetime.combine(datetime.date(1,1,1), datetime.time(0, 0, 0))
    #end = datetime.time(23, 30, 0)
    date_list = [start + datetime.timedelta(minutes=x) for x in range(0, 1440, 30)]
    return date_list

@register.filter
def to_class_name(value):
    return value.__class__.__name__.lower()


def get_rendered_theme(obj):
    res = '';
    try:
        if obj.theme.id:
            th = '.themed-'+ type(obj).__name__ + '-'+ str(obj.id)
            th = th.lower()
            if obj.theme.color:
                res += th+' *{color: '+obj.theme.color+"}\n"
            if obj.theme.background:
                res += th+'{background-color: '+obj.theme.background+"}\n"
            if obj.theme.title_color:
                res += th+' h1,'+th+' h2,'+th+' h3,'+th+' h4{color: '+obj.theme.title_color+"}\n"
            if obj.theme.title_background:
                res += th+' h1,'+th+' h2,'+th+' h3,'+th+' h4{color: '+obj.theme.title_color+"}\n"

            if obj.theme.css:
                css = obj.theme.css
                p = re.compile( "\{theme\}")
                res += p.sub( th, css)
    except:
        pass

    return res


@register.simple_tag(takes_context=True)
def render_all_themes(context):
    ret = ''    
    for obj in CssTheme.objects.all():         
        ret += obj.render_theme()
    
    ret = mark_safe(ret)    
        
    return ret



@register.inclusion_tag('wtpages/includes/subpages_inline.html', takes_context=True)
def get_subpages_inline (context, parent, show_page_header=True, header_style='outside'):
    themed_css = '/* themed css */'
    themed_css += get_rendered_theme(parent)
    items = parent.get_children().live()
    #for item in items:
    for index, item in enumerate(items):
        try:
            page_context = item.specific.get_context(context['request'])

            #try: page_context['request'] = context['request']
            #except: pass

            #TODO: something more flexible
            #try: page_context['is_phone'] = context['is_phone']
            #except:pass
            page_context['parent'] = parent
            page_context['header_style'] = header_style            
            page_context['show_page_header'] = show_page_header            
            page_context['subpage_index'] = 'subpage-'+str(index)

            item.content = render_to_string(item.specific.inline_template, page_context)
            #item.content = render_to_string(item.specific.inline_template, {'self':item.specific})
        except AttributeError: item.content = ''

        themed_css += get_rendered_theme(item)

        try: themed_css += item.specific.render_details_theme()
        except AttributeError: pass

        try: themed_css += item.specific.render_item_theme(item.specific)
        except AttributeError: pass

    parent.themed_css = themed_css

    return {
        'header_style': header_style,
        'subpages': items,
        'parent': parent,
        'request': context['request'],
    }

@register.simple_tag(takes_context=True)
def jscode(context, pos):
    jsc = '<!-- NO REAL CODE IN DEBUG MODE -->'
    if not settings.DEBUG:
        jsc = ''
        for code in JsCode.objects.filter(doc_position=pos):
            jsc += code.text
    if jsc:
        return mark_safe(jsc)
    return '';

@register.simple_tag(takes_context=True)
def youtube_code(context, link):
    #'<iframe width="" height="" src="https://www.youtube.com/embed/'.@$matches[1].'" frameborder="0" allowfullscreen></iframe>';
    # https://www.youtube.com/watch?v=B_8y-lmm08E
    return 'https://www.youtube.com/embed/' + link.replace('https://www.youtube.com/watch?v=','')


@register.assignment_tag(takes_context=True)
def get_social_links(context):
    links = SocialfeedPage.objects.live().first()
    if links:
        links = links.social_items.filter(show_in_links=True)
    
    return links

@register.inclusion_tag('wtpages/includes/social_icons.html', takes_context=True)
def social_icons(context, add_class=''):
    #social_links = SocialfeedPage.objects.all()
    social_links = SocialfeedPage.objects.live().first()
    if social_links:
        social_links = social_links.social_items.filter(show_in_links=True)
    
    return {
        'add_class': add_class,
        'social_links': social_links,
        'request': context['request'],
    }
