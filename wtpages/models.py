# -*- coding: utf-8 -*-
import re

from copy import copy

from django.db import models

#from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django import forms
from django.core.exceptions import ValidationError
from django.apps import apps

from django.template.response import TemplateResponse

#from wagtail.wagtailcore import blocks
#from wagtail.wagtailimages.blocks import ImageChooserBlock

from wagtail.wagtailcore.models import Page, Orderable, PAGE_TEMPLATE_VAR
from wagtail.wagtailcore.fields import RichTextField, StreamField

from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel
from wagtail.wagtailsnippets.edit_handlers import SnippetChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel

from wagtail.wagtailsnippets.models import register_snippet

from wagtail.wagtailsearch import index

from modelcluster.fields import ParentalKey
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase


from django.utils import translation
from django.http import HttpResponse
from django.http import HttpResponseRedirect

from wagtail.wagtailadmin.edit_handlers import TabbedInterface, ObjectList

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtblocks.models import ContentBlockThemed
from django.conf import settings as system_settings

from wtforms.models import WtFormLinkField

SLIDESHOW_STYLE_CHOICES = (
    ('carousel',     _("Carousel")),
    ('fading',       _("Fading")),
    #('fading_eb',    _("Fading EB")),
)

PAGE_LAYOUT_CHOICES = (
    ('left',        _("Left Text Right Image")),
    ('right',       _("Right Text Left Image")),
    ('whole',       _("Only Text")),
    ('empty',       _("Empty Page")),
)


class LanguageRedirectionPage(Page):
    #parent_page_types = ['wagtailcore.Page']
    #subpage_types = ['pages.StandardPage','pages.StandardIndexPage']
    parent_page_types = []
    def serve(self, request):
        language = translation.get_language_from_request(request)
        return HttpResponseRedirect(self.url + language + '/')


class LinkFields(models.Model):
    link_external = models.URLField("External link", blank=True)
    link_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        related_name='+',
    )
    link_document = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        related_name='+'
    )

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        elif self.link_document:
            return self.link_document.url
        else:
            return self.link_external

    panels = [
        FieldPanel('link_external'),
        PageChooserPanel('link_page'),
        DocumentChooserPanel('link_document'),
    ]

    class Meta:
        abstract = True

class CarouselItem(LinkFields):
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    embed_url = models.URLField("Embed URL", blank=True)
    caption = models.TextField(blank=True)
    text = RichTextField(blank=True)
    visible = models.BooleanField(blank=True, default=1)

    panels = [
        ImageChooserPanel('image'),
        #FieldPanel('embed_url'),
        FieldPanel('caption'),
        FieldPanel('text'),
        MultiFieldPanel(LinkFields.panels, "Link"),
        FieldPanel('visible'),
    ]

    class Meta:
        abstract = True


class RelatedLink(LinkFields):
    title = models.CharField(max_length=255, help_text="Link title")
    panels = [
        FieldPanel('title'),
        MultiFieldPanel(LinkFields.panels, "Link"),
    ]
    class Meta:
        abstract = True

#### jjs codes, type kit and similar stuff
#@register_snippet
class JsCodePlacement(models.Model):
    page = ParentalKey('wagtailcore.Page', related_name='js_code_placements')
    jscode = models.ForeignKey('wtpages.JsCode', related_name='+')

DOC_POS = (
    ('doc_end', 'End of document (before </body>)'),
    ('doc_beg', 'Beginning of document (after <body>)'),
    ('head',    'Head (inside <head>)'),
)
@register_snippet
class JsCode(models.Model):
    title = models.TextField(blank=False)
    doc_position = models.CharField(max_length=10, choices=DOC_POS, default='doc_end')
    text = models.TextField(blank=False)
    panels = [
        #FieldPanel('title'),
        #FieldPanel('text'),
        MultiFieldPanel([
                FieldPanel('title'),
                FieldPanel('doc_position'),
                FieldPanel('text'),
            ],
            heading=_("All Pages JavaScript code"),
            #heading=_("JavaScript код для всех страниц"),
            #classname="collapsible collapsed"
        ),
    ]
    def __str__(self):
        return self.title


BLOCK_WIDTH = (
    ('12','100%'),
    ('9','75%'),
    ('6','50%'),
    ('3','25%'),
)
#@register_snippet
class FooterText(models.Model):
    title = models.TextField(blank=False)
    text = models.TextField(blank=False)
    block_width = models.CharField(max_length=10, choices=BLOCK_WIDTH, default='6')
    sort_order = models.IntegerField(blank=True, default=0)
    panels = [
        #FieldPanel('title'),
        #FieldPanel('text'),
        MultiFieldPanel([
                FieldPanel('title'),
                FieldPanel('text'),
                FieldPanel('block_width'),
                FieldPanel('sort_order'),
            ],
            #heading=_("All Pages JavaScript code"),
            heading=_("Text in footer"),
            #classname="collapsible collapsed"
        ),
    ]
    def __str__(self):
        return self.title
    class Meta:
        ordering = ('sort_order',)

@register_snippet
class CssTheme(Orderable):
    class Meta:
        verbose_name = _('Css Theme')
        verbose_name_plural=_('Css Themes')
        ordering = ('title',)        
        
    title = models.CharField(max_length=255)

    color = models.CharField(max_length=255, blank=True)
    background = models.CharField(max_length=255, blank=True)
    title_color = models.CharField(max_length=255, blank=True)
    title_background = models.CharField(max_length=255, blank=True)

    css = models.TextField(verbose_name = _('Css'), blank=True, help_text=_('Raw CSS. There are NO validation, please be careful. {theme} should precede any selector.'))

    def __str__(self):
        return self.title

    def render_theme(self):
        res = '';
        try:
            if self.id:
                th = '.themed-'+ str(self.id)
                th = th.lower()
                if self.color:
                    res += th+' *{color: '+self.color+"}\n"
                if self.background:
                    res += th+'{background-color: '+self.background+"}\n"
                if self.title_color:
                    res += th+' h1,'+th+' h2,'+th+' h3,'+th+' h4{color: '+self.title_color+"}\n"
                if self.title_background:
                    res += th+' h1,'+th+' h2,'+th+' h3,'+th+' h4{background-color: '+self.title_color+"}\n"

                if self.css:
                    css = self.css
                    p = re.compile( "\{theme\}")
                    res += p.sub( th, css)
        except:
            pass

        return res



    panels = [
        MultiFieldPanel([
                FieldPanel('title', classname=""),
                FieldPanel('color'),
                FieldPanel('background'),
                FieldPanel('title_color'),
                FieldPanel('title_background'),
                FieldPanel('css', classname=""),
            ],
            heading=_("Basic"),
        ),
    ]

class LayoutablePage(models.Model):
    class Meta:
        abstract = True
    page_layout = models.CharField(max_length=255, default='left', choices=PAGE_LAYOUT_CHOICES, blank=True)
    panels = [        
        FieldPanel('page_layout'),
    ]
    

# PAGES
class WtBasePage(Page):
    class Meta:
        abstract = True
    seo_keywords = models.TextField(verbose_name=_('SEO keywords'), blank=True)     
    theme = models.ForeignKey(
        'wtpages.CssTheme',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name = _("Theme")
    )
    
    show_separator = models.BooleanField(default=False)
    
    open_graph_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    
    def get_sitemap_urls(self):
        flg = self.get_parent().specific.__dict__.get('subpages_inline', False) or self.get_parent().specific.__class__.__dict__.get('subpages_inline', False) 
        return [] if flg else super(WtBasePage, self).get_sitemap_urls()

    def render_item_theme(self, obj):
        res = '';
        try:
            if obj.theme.id:
                th = '.themed-'+ type(obj).__name__ + '-'+ str(obj.id)
                th = th.lower()
                if obj.theme.color:
                    res += th+' *{color: '+obj.theme.color+"}\n"
                if obj.theme.background:
                    res += th+'{background-color: '+obj.theme.background+"}\n"
                if obj.theme.title_color:
                    res += th+' h1,'+th+' h2,'+th+' h3,'+th+' h4{color: '+obj.theme.title_color+"}\n"
                if obj.theme.title_background:
                    res += th+' h1,'+th+' h2,'+th+' h3,'+th+' h4{color: '+obj.theme.title_color+"}\n"

                if obj.theme.css:
                    css = obj.theme.css
                    p = re.compile( "\{theme\}")
                    res += p.sub( th, css)
        except:
            pass

        return res
    
    promote_panels = [
        MultiFieldPanel([
            FieldPanel('show_in_menus'),
            FieldPanel('slug'),
            FieldPanel('seo_title'),
            FieldPanel('search_description'),
            FieldPanel('seo_keywords'),
            ImageChooserPanel('open_graph_image'),
        ], _('Common page configuration')),
    ]


class WtBaseIndexPage(WtBasePage):
    class Meta:
        abstract = True
    subpages_inline = models.BooleanField(default=False, help_text='Show subpages content inline')
    hide_dropdown = models.BooleanField(default=False, help_text='Hide subpages from top navigation')
    redirect_to_first_child = models.BooleanField(default=False, help_text='Show the first child page instead of showing this one')

    def clean(self):
        super(WtBaseIndexPage, self).clean()
        if self.redirect_to_first_child and self.subpages_inline:
            raise ValidationError({'redirect_to_first_child': _(
                "Subpages Inline and Redirect to "
                " First Child can not be selected together "
                )})


    def serve(self, request, *args, **kwargs):
        if self.redirect_to_first_child:
            c = self.get_children().live().first()
            if c:
                return c.specific.serve(request, *args, **kwargs)        
        return super(WtBaseIndexPage, self).serve(request, *args, **kwargs)


    promote_panels = [
        MultiFieldPanel([
            FieldPanel('show_in_menus'),
            FieldPanel('subpages_inline'),
            FieldPanel('redirect_to_first_child'),
            FieldPanel('hide_dropdown'),
            FieldPanel('slug'),
            FieldPanel('seo_title'),
            FieldPanel('search_description'),
            FieldPanel('seo_keywords'),
        ], _('Common page configuration')),
    ]


class URLPage(Page):
    class Meta:
        verbose_name = _("URL Page")
    link_external = models.URLField(blank=True, null=True)
    external = models.BooleanField(default=False, help_text='Open link in a new window')
    
    link_page = models.ForeignKey(
        'wagtailcore.Page',
        null=True,
        blank=True,
        related_name='+',
        on_delete=models.SET_NULL,    
    )
    link_document = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        related_name='+',
        on_delete=models.SET_NULL,
    )

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        elif self.link_document:
            return self.link_document.url
        else:
            return self.link_external

    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="title"),
            FieldPanel('link_external'),
            PageChooserPanel('link_page'),
            DocumentChooserPanel('link_document'),
            FieldPanel('external'),
        ],heading=_('Basic')),
    ]
    promote_panels = [
        MultiFieldPanel([
            FieldPanel('show_in_menus'),
            FieldPanel('slug'),
            #FieldPanel('seo_title'),
            #FieldPanel('search_description'),
            #FieldPanel('seo_keywords'),
        ], _('Common page configuration')),
    ]


class StandardIndexPageCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('wtpages.StandardIndexPage', related_name='carousel_items')
class StandardIndexPageSideCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('wtpages.StandardIndexPage', related_name='side_carousel_items')    
class StandardIndexPageBottomCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('wtpages.StandardIndexPage', related_name='bottom_carousel_items')    
class StandardIndexPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('wtpages.StandardIndexPage', related_name='related_links')

class StandardIndexPageFormLink(Orderable, WtFormLinkField):
    page = ParentalKey('wtpages.StandardIndexPage', related_name='form_links')

class StandardIndexPage(WtBaseIndexPage, LayoutablePage):
    class Meta:
        verbose_name = _("Index Page")

    template = 'wtpages/standard_page.html'

    body = RichTextField(blank=True, verbose_name = _("Text"))
    raw_html = models.TextField(blank=True, null=True,)
    carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    hide_title = models.BooleanField(default=False)
    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    
    
    side_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    bottom_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)

    @property
    def navbar_items(self):        
        return self.get_children().live().in_menu()
    
    @property
    def top_page_title(self):        
        return self.title
    
    @property
    def top_page(self):
        return self

    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('page_heading', classname="full title"),
            FieldPanel('page_content_heading', classname="full title"),            
            FieldPanel('body'),
            FieldPanel('raw_html'),            
            FieldPanel('page_layout'),
            FieldPanel('hide_title'),
            #FieldPanel('show_separator'),
            SnippetChooserPanel('theme'),
        ],heading=_('Basic')),
        MultiFieldPanel([
            FieldPanel('carousel_style'),
            InlinePanel('carousel_items'),],heading=_('Top Images'),classname="collapsible collapsed"),
        MultiFieldPanel([
            FieldPanel('side_carousel_style'),
            InlinePanel('side_carousel_items'),],heading=_('Side Images'),classname="collapsible collapsed"),              
        MultiFieldPanel([
            FieldPanel('bottom_carousel_style'),
            InlinePanel('bottom_carousel_items'),],heading=_('Bottom Images'),classname="collapsible collapsed"),              
        MultiFieldPanel([InlinePanel('related_links'),],heading=_('Related Links'),classname="collapsible collapsed"),
        MultiFieldPanel([InlinePanel('form_links'),],heading=_('Forms'),classname="collapsible collapsed"),
    ]




class StandardPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('wtpages.StandardPage', related_name='carousel_items')
    
class StandardPageSideCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('wtpages.StandardPage', related_name='side_carousel_items')
    
class StandardPageBottomCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('wtpages.StandardPage', related_name='bottom_carousel_items')    
        
class StandardPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('wtpages.StandardPage', related_name='related_links')
class StandardPageFormLink(Orderable, WtFormLinkField):
    page = ParentalKey('wtpages.StandardPage', related_name='form_links')




class StandardPage(WtBasePage, LayoutablePage):
    class Meta:
        verbose_name = _("Text Page")
    
    inline_template = 'wtpages/standard_page_inline.html'
    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    body = RichTextField(blank=True, verbose_name = _("Text"))
    
    raw_html = models.TextField(blank=True, null=True,)
    
    hide_title = models.BooleanField(default=False)
    show_index_title = models.BooleanField(default=False)    
    
    carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    side_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    bottom_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    @property
    def navbar_items(self):
        return self.get_parent().get_children().live().in_menu()
    
    @property
    def top_page_title(self):        
        return self.get_parent().title
    
    @property
    def top_page(self):
        return self.get_parent()
        
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('page_heading', classname="full title"),
            FieldPanel('page_content_heading', classname="full title"),
            
            FieldPanel('page_layout'),
            FieldPanel('body'),
            FieldPanel('raw_html'),
                        
            FieldPanel('show_index_title'),
            FieldPanel('hide_title'),
            FieldPanel('show_separator'),
            SnippetChooserPanel('theme'),
        ],heading=_('Basic')),
        MultiFieldPanel([
            FieldPanel('carousel_style'),
            InlinePanel('carousel_items'),],heading=_('Top Images'),classname="collapsible collapsed"),
        MultiFieldPanel([
            FieldPanel('side_carousel_style'),
            InlinePanel('side_carousel_items'),],heading=_('Side Images'),classname="collapsible collapsed"),        
        MultiFieldPanel([
            FieldPanel('bottom_carousel_style'),
            InlinePanel('bottom_carousel_items'),],heading=_('Bottom Images'),classname="collapsible collapsed"),              
        MultiFieldPanel([InlinePanel('related_links'),], heading=_('Related Links'), classname="collapsible collapsed"),
        MultiFieldPanel([InlinePanel('form_links'),], heading=_('Forms'), classname="collapsible collapsed"),
    ]
    
    promote_panels = [
        MultiFieldPanel([            
            FieldPanel('slug'),
            FieldPanel('show_in_menus'),
            FieldPanel('seo_title'),
            FieldPanel('search_description'),
            FieldPanel('seo_keywords'),
        ], _('Common page configuration')),
    ]
