# -*- coding: utf-8 -*-
from .base import *

DEBUG = True
#ALLOWED_HOSTS=['*']
LOGGING = {}
#TEMPLATE_DEBUG = True

#TEMPLATES['OPTIONS']['debug'] = 'DEBUG';
TEMPLATES[0]['OPTIONS']['debug'] = True

#SECRET_KEY = 'a)jc_j=q0l5=!ibo!_=4^u$-w+!chf+&xe3vu$@a1mus#fnpmd'


EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


try:
    from .local import *
except ImportError:
    pass
