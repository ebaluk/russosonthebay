# -*- coding: utf-8 -*-
from .base import *


DEBUG = False
TEMPLATES[0]['OPTIONS']['debug'] = False
#ALLOWED_HOSTS=['127.0.0.1']
ALLOWED_HOSTS=['*']

#X_FRAME_OPTIONS = 'DENY'
#SECURE_BROWSER_XSS_FILTER = True
#CSRF_COOKIE_HTTPONLY = True
#SESSION_COOKIE_SECURE = True
#CSRF_COOKIE_SECURE = True
#SECURE_CONTENT_TYPE_NOSNIFF = True
#SECURE_SSL_REDIRECT = True
CSRF_COOKIE_DOMAIN = True


try:
    from .local import *
except ImportError:
    pass
