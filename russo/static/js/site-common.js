var History = History || {};
$.extend(History,{
    pathname :null,
    previousHash :null,
    hashCheckInterval : -1,
    stack : [],
    initialize :function () {
        var that = this;
        if (that.supportsHistoryPushState()) {
            that.pathname = document.location.pathname;
            $(window).off("popstate").on("popstate", that.onHistoryChanged);
        } else {
            that.hashCheckInterval = setInterval(that.onCheckHash, 200);
        }
    },
    navigateToPath : function(pathname) {
        if(pathname == '') pathname = '#';
        var h = pathname.split("#")[1];
        var $aa = $('.menu a[href="#'+h+'"]');
        if($aa.length > 0){
            on_navigate_click($aa);
        }
    },
    supportsHistoryPushState : function () {
        return ("pushState" in window.history) && window.history.pushState !== null;
    },
    onCheckHash : function () {
        if (document.location.hash !== this.previousHash) {
            this.navigateToPath(document.location.hash.slice(1));
            this.previousHash = document.location.hash;
        }
    },
    pushState : function (url) {
        if (this.supportsHistoryPushState()) {
            window.history.pushState("", "", url);
        } else {
            this.previousHash = url;
            document.location.hash = url;
        }
        this.stack.push(url);
    },
    onHistoryChanged : function (event) {
        if (History.supportsHistoryPushState()) {
            if(History.pathname != document.location.href){
                History.pathname = null;
                History.navigateToPath( document.location.hash );
            }
        }
    }
});
