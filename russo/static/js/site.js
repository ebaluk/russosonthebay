var windowHeight;
var windowWidth;
var $window = $(window);
var cnk_idx = function(idx, dir, len){
    if(dir > 0){ if(idx >= len) return idx - len; }
    else{ if(idx < 0) return len + idx; }
    return idx;
}

var global_waiting = function(){$('body').append('<div class="waiting-wrapper"><i class="waiting fa fa-circle-o-notch fa-spin"></i></div>');}
var global_waiting_stop = function(){$('.waiting-wrapper').remove();}


$(function(){
    $window.on('resize', function(){
        windowHeight = $(this).height();
        windowWidth = $(this).width();
//         $('.slide-small').css('height', '580px');
//         $('.slide-big').css('height', (windowHeight * 0.95) + 'px');
//         $('.slide-full').css('height', windowHeight + 'px');
    }).trigger('resize');

    $('.screen-down a').on('click', function(){
        //$(window).scrollTop( $(window).scrollTop() + windowHeight);
        $('body,html').animate({scrollTop: $(window).scrollTop() + (windowHeight*0.95)},1000);
    });

    $('.body-content img').addClass("img-thumbnail");

    var $ttt = $('.js-scroll-top');
    var $scd = $('.screen-down:not(.screen-down-home)');
    var $header = $('header');

    $(window).on('scroll', function(){
        if( $(window).scrollTop() > 20 ){
            $ttt.fadeIn();
            $scd.fadeIn();
            $header.addClass('solid');
        }
        else{
            $header.removeClass('solid');
            $ttt.fadeOut();
            $scd.fadeOut();
        }
    }).trigger('scroll');

    $ttt.on('click', function(){
        //$(window).scrollTop(0);
        $('body,html').animate(
            {scrollTop: 0},
            1000,
            function(){
                $ttt.fadeOut();
                $scd.fadeOut();
            }
        );
    });
    if (!!$.prototype.columnize){
        $('.container-block.block-text .rich-text').columnize({columns:2}).removeClass('columnize').addClass('columnized');
        $(".columnize:visible .rich-text").columnize({columns:2}).removeClass('columnize').addClass('columnized');
        //$(".columnize_search_form:visible").columnize({columns:2}).removeClass('columnize_search_form').addClass('columnized');
    }
    if (!!$.prototype.datepicker){
        $('.date-pick').datepicker({autoclose: true});
    }
});

$(function(){
    //iscroller
    if (!!$.prototype.iscroller){
        $(".iscroller").iscroller().on("iscroller.update.end", function(){
        	if (!!$.prototype.modal_carousel){
                $(".modal-carousel", this).modal_carousel({modalCarouselClass: "portfolio-modal", showDots: false});
            }
        });
    }
    
    if (!!$.prototype.modal_carousel){
        $(".modal-carousel:not(.modal-carousel-source)").modal_carousel({modalCarouselClass: "portfolio-modal", showDots: false});
    }
    
    $('.js-filter-wrapper .js-filter-contol').on('click', function(event){
    	event.preventDefault();
        var fstr = $(this).closest('.js-filter-wrapper').data('filter');
        var val = $(this).data('filter-val');
        var $fstr = $(fstr);
        $fstr.addClass('hidden');
        $(fstr+"[data-filtered~='"+val+"']").removeClass('hidden');
        $('.js-filter-wrapper .js-filter-contol').removeClass('active');
        $(this).addClass('active');

        if (!!$.prototype.iscroller){
            $(".iscroller").iscroller("update", true);
            $(".iscroller").trigger("filter");
        }

    });
});

$(function(){
    var odd = true;
    var cl = {true:'odd',false:'even'};
    $('.subpage').each(function(){
        $(this).removeClass('odd').removeClass('even').addClass(cl[odd]);
        odd = ! odd;
    })
});

$(function(){
    if (!!$.prototype.ekkoLightbox){
        $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    }
});

// ticker
$(function(){
    var
        $bw = $(".ticker-wrapper"),
        $b  = $(".ticker-body", $bw),
        $ul = $(".ticker-body ul", $bw),
        scrollTimer = 0,
        dir = 1,
        delta = 1,
        sl = -10
    ;

    $b.append( $b.html() ).append( $b.html() ).append( $b.html() ).append( $b.html() );

    var scrollBrands = function(){
        var cs = $b.scrollLeft();
        $b.scrollLeft(cs  + dir * delta);
        if($b.scrollLeft() == sl){
            $b.scrollLeft( 0 );
            sl = -10;
        }
        else{
            sl = $b.scrollLeft();
        }
        scrollTimer = setTimeout(scrollBrands, 50);
    };
    if($bw.length){
        clearTimeout(scrollTimer);
        dir = 1;
        scrollBrands();

        $b.hover(
            function(){
                clearTimeout(scrollTimer);
            },
            function(){
                scrollBrands();
            }
        )
        ;
    }
});


$(function(){
//     $('.modal').on('bs.show', function(){
//     });
    $('.widget-dateinput input').datepicker({autoclose: true});

    $('.js-move-to-body').each(function(){
        $(this).detach().appendTo($('body'));
    });
});

$(function(){
    $('#formEvent').on('show.bs.modal', function (e) {
        var $a = $(e.relatedTarget);
        var $t = $(this);
        $('.modal-title', $t).text( $a.data('title') );
        $('.modal-body', $t).html( $($a.data('id')).html() );
    })
    
    //$fp = 
    $('#formThanks').clone().attr('id','formPopup').appendTo('body');
    
    $('#formPopup').on('show.bs.modal', function (e) {
        var $a = $(e.relatedTarget);
        var $t = $(this);
        //$('.modal-title', $t).text( $a.data('title') );        
        
        if( "google-api-map" == $a.data('type') )
        {
        	var $gm = $("<div/>")
        	.addClass("embed-responsive embed-responsive-16by9 google-api-map")
        	.data("title", $a.data("title"))
        	.data("info", $a.data("info"))
        	.data("latitude", $a.data("latitude"))
        	.data("longitude", $a.data("longitude"));        	
        	
        	$('.modal-body', $t).html( $gm );
        	
        }     
        else if( $a.data('iframe') )
        {
            var $ih = $('<iframe></iframe>')
                .attr('src', $a.data('iframe'))
                .attr('width', 500)
                .attr('height', 280)
                .css({'width': '100%'})
            ;
            $('.modal-body', $t).html( $ih );
            
            if( $a.data('title') )
                $('.modal-title',$t).html($a.data('title'));
        }
        else{
            $('.modal-body', $t).html( $a.attr('href') );
        }
    }).on('shown.bs.modal', function (e) {
    	var $a = $(e.relatedTarget);
    	if( "google-api-map" == $a.data('type') )
    	{
    		$(".google-api-map", this).eb_google_map();	
    	}    	
    });
});


$(function(){
	$(".carousel.slideshow.fading .carousel-image").each(function(){		
		var re  = /url\("([^"]+)"\)/i
		var res = re.exec($(this).css("backgroundImage"));
		if(res)
		{
			console.log("preloading: " + res[1]);
			$("<img/>").attr("src", res[1]);			
		}
	});
});


(function ($){

	var 
	
	defaults = {
		key: "AIzaSyA9svStjd0grfkvYguszJwxtBtXx1fAqh8",
		style:	[{"featureType":"administrative","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"geometry.fill","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.locality","elementType":"geometry.stroke","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative.locality","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"labels.text.stroke","stylers":[{"visibility":"on"}]},{"featureType":"administrative.locality","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"poi.attraction","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi.attraction","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"poi.attraction","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"poi.attraction","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi.attraction","elementType":"labels.text.stroke","stylers":[{"visibility":"on"}]},{"featureType":"poi.attraction","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"labels.text.stroke","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"poi.government","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"poi.government","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi.government","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"poi.government","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"poi.government","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi.government","elementType":"labels.text.stroke","stylers":[{"visibility":"on"}]},{"featureType":"poi.government","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","elementType":"geometry.stroke","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","elementType":"labels.text","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","elementType":"labels.text.stroke","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","elementType":"labels.icon","stylers":[{"visibility":"on"}]}]
	},
	
	methods = {		
		init: function(options){
			
			var that = this;
			
			var settings = $.extend( {}, defaults, options || {} );
			
			if ( ! ( typeof google === 'object' && typeof google.maps === 'object' ) )
			{
				$.ajax({
					  url: "https://maps.googleapis.com/maps/api/js?key="+settings.key,
					  dataType: "script",
					  success: function(){
						  methods.init.apply( that, arguments );
					  }
				});
        	}
			else
			{
				that.each(function(){		
					var $this = $(this);			
					var myLatLng = {lat: $this.data("latitude"), lng: $this.data("longitude")}; 
					var map=new google.maps.Map($this[0],{
						center: myLatLng,    
					    zoom: 16,
					    scrollwheel: false,
					    mapTypeId:google.maps.MapTypeId.ROADMAP,
					    styles: $this.data("styles") ? $this.data("styles") : settings.styles
					});
					var marker = new google.maps.Marker({
						position: myLatLng,
						map: map,
						title: $this.data("title"),
					});

					var infowindow = new google.maps.InfoWindow({
						content: $this.data("info"),
					});

					marker.addListener('click', function() {
						infowindow.open(map, marker);
					});					
					
				});			
			}
			
			return that;
		}
	};
	
	$.fn.eb_google_map = function(method){
		return methods.init.apply( this, arguments );
	};
	
}(jQuery));


$(function(){
	$(".google-api-map").eb_google_map();	 
});

$(function(){
	$('nav.navbar.navbar-static-top')
	.on('affix.bs.affix', function (e) {
		$("body").addClass("nav-affix");		
	})
	.on('affix-top.bs.affix', function (e) {
		$("body").removeClass("nav-affix");		
	});
	
});

$(function(){
    
    function footerAffix(){
        var sectionsHeight = 0;
        $("section, footer, .page-header.outside").each(function(){
            sectionsHeight += $(this).outerHeight();
        });
        
        if( $("footer").hasClass("fixed") )
        {
            if( $("body").innerHeight() < sectionsHeight )
            {
                $("footer").removeClass("fixed");
            }
        }
        else
        {
            if( $("body").innerHeight() >= sectionsHeight )
            {
                $("footer").addClass("fixed");
            }
        }
    }
        
    
    footerAffix();
            
    $(window).resize(function(){
    	footerAffix();
    });


});


$(function() {
	$(".carousel").swipe( {
		//Generic swipe handler for all directions
		swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
			if (direction == 'left') $(this).carousel('next');
		    if (direction == 'right') $(this).carousel('prev');
		}
	});
});
