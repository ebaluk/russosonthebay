# -*- coding: utf-8 -*-
from django.conf.urls import include, url
from django.conf import settings
from django.contrib import admin

from wagtail.wagtailadmin import urls as wagtailadmin_urls
from wagtail.wagtaildocs import urls as wagtaildocs_urls
from wagtail.wagtailcore import urls as wagtail_urls

#from wagtail.wagtailcore.views import serve

#from wagtail.contrib.wagtailapi import urls as wagtailapi_urls

from wtinstagramtokens.views import instagram_config

from wtforms.views import WtFormView

from wtadmin import urls as wtadmin_urls
from wtsitemap import urls as wtsitemap_urls

from wt404test import urls as wt404test_urls 

urlpatterns = wtadmin_urls.urlpatterns + wtsitemap_urls.urlpatterns + wt404test_urls.urlpatterns +  [ 
    url(r'^django-admin/', include(admin.site.urls)),
    url(r'^admin/', include(wagtailadmin_urls)),
    url(r'^instagram_config/', instagram_config, name='instagram_config'),
    url(r'^wtform/(?P<pk>\d+)/$', WtFormView.as_view()),    
    
    url(r'^documents/', include(wagtaildocs_urls)),
    #url(r'^search/$', 'search.views.search', name='search'),
    #url(r'^api/', include(wagtailapi_urls)),
    url(r'^captcha/', include('captcha.urls')),
    url(r'', include(wagtail_urls)),    
]


if settings.DEBUG:
#if 1:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    #from django.views.generic import TemplateView

    # Serve static and media files from development server
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
