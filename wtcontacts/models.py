# -*- coding: utf-8 -*-
from django.db import models
from django.core.cache import cache


from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailforms.models import AbstractFormField

from modelcluster.fields import ParentalKey

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtpages.models import CarouselItem, RelatedLink, LayoutablePage, SLIDESHOW_STYLE_CHOICES
from wtblocks.models import ContentBlockThemed
from wtpages.models import *

import urllib, json

from urllib.parse import urlencode

from django.conf import settings
from compressor.utils.decorators import cached_property
from django.utils.dates import WEEKDAYS
import datetime

from wtforms.models import WtFormLinkField
from django.template.defaultfilters import default

###
# Contact page

class ContactFields(models.Model):
    name = models.CharField(max_length=255, blank=False)
    country = models.CharField(max_length=255, blank=False)
    city = models.CharField(max_length=255, blank=True )
    state = models.CharField(max_length=255, blank=True, )
    zip_code = models.CharField(max_length=5, blank=True, verbose_name=_('Zip'))
    address_1 = models.CharField(max_length=255, blank=True)
    address_2 = models.CharField(max_length=255, blank=True)
    phone = models.CharField(max_length=20, blank=True, verbose_name=_('Phone'))
    fax = models.CharField(max_length=20, blank=True, verbose_name=_('Fax'))
    email = models.EmailField(blank=True)
    map_code = models.TextField(blank=True, verbose_name=_('Map'))
    open_table_id = models.IntegerField(blank=True, null=True)
    yahoo_woeid = models.IntegerField(blank=True, null=True, verbose_name=_('Yahoo WOEID'))
    
    latitude = models.CharField(max_length=100, blank=True, null=True)
    longitude = models.CharField(max_length=100, blank=True, null=True)
    
    map_style = models.TextField(blank=True, verbose_name=_('Map Style'), help_text='Go to the https://snazzymaps.com/explore find required theme and copy/paste it here')
    
    vcf_card = models.ForeignKey(
        'wagtaildocs.Document',
        null=True,
        blank=True,
        related_name='+',
        on_delete=models.SET_NULL
    )

    panels = [
        FieldPanel('name'),
        #FieldPanel('country'),
        FieldPanel('city'),
        FieldPanel('state'),
        FieldPanel('zip_code'),
        FieldPanel('address_1'),
        FieldPanel('address_2'),
        FieldPanel('phone'),
        FieldPanel('fax'),
        FieldPanel('email'),
        FieldPanel('map_code'),
        #FieldPanel('open_table_id'),
        #FieldPanel('yahoo_woeid'),        
        FieldPanel('latitude'),
        FieldPanel('longitude'),
        FieldPanel('map_style'),
        
        #DocumentChooserPanel('vcf_card'),        
        
    ]

    class Meta:
        abstract = True

class ContactPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('wtcontacts.ContactPage', related_name='carousel_items')
#class HoursImage(Orderable, CarouselItem):
    #page = ParentalKey('wtcontacts.ContactPage', related_name='hours_images')

    
    
class ContactPageSideCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('wtcontacts.ContactPage', related_name='side_carousel_items')

class ContactPageFormLink(Orderable, WtFormLinkField):
    page = ParentalKey('wtcontacts.ContactPage', related_name='form_links')
    

class ContactHours(Orderable, models.Model):    
    page = ParentalKey('wtcontacts.ContactPage', related_name='contact_hours')
    title = models.CharField(blank=True, null=True, max_length=255, )    
    hours = models.TextField(blank=False)
    column_break = models.BooleanField(default=False)    
    panels = [        
        FieldPanel('title'),
        FieldPanel('hours'),
        FieldPanel('column_break'),        
    ]
    

class ContactPage(WtBasePage, ContactFields, LayoutablePage):
    body = RichTextField(blank=True, verbose_name = _("Text"))
    hide_title = models.BooleanField(default=False)
    show_index_title = models.BooleanField(default=False)
    
    default_contact = models.BooleanField(default=False)
    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    
    
    carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    side_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    hours = RichTextField(blank=True, verbose_name=_('Full'))
    hours_short = RichTextField(blank=True, verbose_name=_('Short'), help_text=_('Showing on footer'))
    
    
    #inline_page = models.ForeignKey(
        ##'wagtailcore.Page',
        #'wtforms.FormPage',
        #null=True,
        #blank=True,
        #related_name='+',
        #verbose_name="Форма обратной связи",
    #)
    class Meta:
        verbose_name = _("Contacts")
        
    @cached_property
    def google_map_info(self):
        ret = '<div style="text-align: center">';
        if self.name:
            ret = ret + '<h1>' + self.name  + '</h1>'
        if self.address_1:
            ret = ret +  self.address_1  + '<br>'             
        if self.address_2:
            ret = ret +  self.address_2            
        if self.city:    
            ret = ret +  self.city + ', '            
        if self.state:
            ret = ret +  self.state + ' '
        if self.zip_code:
            ret = ret +  self.zip_code         
        ret = ret + '</div>';
        return ret
                  
        

    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('page_heading', classname="full title"),
            FieldPanel('page_content_heading', classname="full title"),
            FieldPanel('page_layout'),
            FieldPanel('body'),
            FieldPanel('show_index_title'),
            FieldPanel('hide_title'),
            FieldPanel('default_contact'),            
        ],heading=_('Basic'),classname="collapsible collapsed-no"),
        MultiFieldPanel(ContactFields.panels,  _("Address")),
        MultiFieldPanel([
                FieldPanel('hours'),
                FieldPanel('hours_short'),                                
            ],heading=_('Hours Of Operation'),classname="collapsible collapsed1111"),
        MultiFieldPanel([
                InlinePanel('contact_hours'),                                
            ],heading=_('Hours Of Operation'),classname="collapsible collapsed1111"),                      
        MultiFieldPanel([
            FieldPanel('carousel_style'),
            InlinePanel('carousel_items'),],heading=_('Top Images'),classname="collapsible collapsed"),
        MultiFieldPanel([
            FieldPanel('side_carousel_style'),
            InlinePanel('side_carousel_items'),],heading=_('Side Images'),classname="collapsible collapsed"),       
        
        MultiFieldPanel([InlinePanel('form_links'),], heading=_('Forms'), classname="collapsible collapsed"),
        
        

        #MultiFieldPanel([InlinePanel('hours_images'),],heading=_('Page Images'),classname="collapsible collapsed"),
        #PageChooserPanel('inline_page'),
    ]
