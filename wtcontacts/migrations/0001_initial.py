# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-10 11:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('wagtailcore', '0032_add_bulk_delete_page_permission'),
        ('wagtailimages', '0017_reduce_focal_point_key_max_length'),
        ('wagtaildocs', '0007_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactHours',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('title', models.CharField(max_length=255)),
                ('hours', models.TextField()),
                ('column_break', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ContactPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('page_layout', models.CharField(blank=True, choices=[('left', 'Left Text Right Image'), ('right', 'Right Text Left Image'), ('whole', 'Only Text'), ('empty', 'Empty Page')], default='left', max_length=255)),
                ('seo_keywords', models.TextField(blank=True, verbose_name='SEO keywords')),
                ('name', models.CharField(max_length=255)),
                ('city', models.CharField(blank=True, max_length=255)),
                ('state', models.CharField(blank=True, max_length=255)),
                ('zip_code', models.CharField(blank=True, max_length=5, verbose_name='Zip')),
                ('address_1', models.CharField(blank=True, max_length=255)),
                ('address_2', models.CharField(blank=True, max_length=255)),
                ('phone', models.CharField(blank=True, max_length=20, verbose_name='Phone')),
                ('fax', models.CharField(blank=True, max_length=20, verbose_name='Fax')),
                ('email', models.EmailField(blank=True, max_length=254)),
                ('map_code', models.TextField(blank=True, verbose_name='Map')),
                ('open_table_id', models.IntegerField(blank=True, null=True)),
                ('yahoo_woeid', models.IntegerField(blank=True, null=True, verbose_name='Yahoo WOEID')),
                ('latitude', models.CharField(blank=True, max_length=100, null=True)),
                ('longitude', models.CharField(blank=True, max_length=100, null=True)),
                ('map_style', models.TextField(blank=True, help_text='Go to the https://snazzymaps.com/explore find required theme and copy/paste it here', verbose_name='Map Style')),
                ('body', wagtail.wagtailcore.fields.RichTextField(blank=True, verbose_name='Text')),
                ('hide_title', models.BooleanField(default=False)),
                ('show_index_title', models.BooleanField(default=False)),
                ('default_contact', models.BooleanField(default=False)),
                ('page_heading', models.CharField(blank=True, help_text='The page content heading', max_length=255)),
                ('carousel_style', models.CharField(blank=True, choices=[('carousel', 'Carousel'), ('fading', 'Fading')], default='carousel', max_length=255)),
                ('side_carousel_style', models.CharField(blank=True, choices=[('carousel', 'Carousel'), ('fading', 'Fading')], default='carousel', max_length=255)),
                ('hours', wagtail.wagtailcore.fields.RichTextField(blank=True, verbose_name='Full')),
                ('hours_short', wagtail.wagtailcore.fields.RichTextField(blank=True, help_text='Showing on footer', verbose_name='Short')),
            ],
            options={
                'verbose_name': 'Contacts',
            },
            bases=('wagtailcore.page', models.Model),
        ),
        migrations.CreateModel(
            name='ContactPageCarouselItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('link_external', models.URLField(blank=True, verbose_name='External link')),
                ('embed_url', models.URLField(blank=True, verbose_name='Embed URL')),
                ('caption', models.TextField(blank=True)),
                ('text', wagtail.wagtailcore.fields.RichTextField(blank=True)),
                ('visible', models.BooleanField(default=1)),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ContactPageFormLink',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('is_popup', models.BooleanField(default=False, verbose_name='Popup Form')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ContactPageSideCarouselItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('link_external', models.URLField(blank=True, verbose_name='External link')),
                ('embed_url', models.URLField(blank=True, verbose_name='Embed URL')),
                ('caption', models.TextField(blank=True)),
                ('text', wagtail.wagtailcore.fields.RichTextField(blank=True)),
                ('visible', models.BooleanField(default=1)),
                ('image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.Image')),
                ('link_document', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtaildocs.Document')),
                ('link_page', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtailcore.Page')),
                ('page', modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='side_carousel_items', to='wtcontacts.ContactPage')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
        ),
    ]
