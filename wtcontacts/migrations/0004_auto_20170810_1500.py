# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-10 15:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wtcontacts', '0003_auto_20170510_1150'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactpage',
            name='country',
            field=models.CharField(default='United Kindom', max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contactpage',
            name='state',
            field=models.CharField(blank=True, max_length=255, verbose_name='District'),
        ),
    ]
