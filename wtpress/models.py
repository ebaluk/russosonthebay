# -*- coding: utf-8 -*-
from django.db import models
from django import forms

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel

from wagtail.wagtailimages.models import Image

from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtpages.models import CarouselItem, RelatedLink
from wtblocks.models import ContentBlockThemed
from wtpages.models import *



COLLECTION_NAME = 'Press'

class PressPage(WtBasePage):
    subpage_types = []
    body = RichTextField(blank=True)

    auto_create_urls = models.TextField(
        blank=True,
        help_text=_("Autoclipper URLS. Each URL should strart from the new line. Protocol (http:// or https://) must be a part of the URL. Press items will be created on PUBLISH this page.")
    )

    inline_template = 'wtpress/includes/press.html'
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('body', classname="full "),
            FieldPanel('theme'),
            FieldPanel('auto_create_urls'),
        ],heading=_('Basic')),
        MultiFieldPanel([InlinePanel('press_items'),],heading=_("Press Items"), classname="collapsible collapsed"),
        MultiFieldPanel([InlinePanel('carousel_items'),],heading=_('Images'), classname="collapsible collapsed"),
    ]



class PressPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('wtpress.PressPage', related_name='carousel_items')
class PressItem(Orderable, models.Model):
    page = ParentalKey('wtpress.PressPage', related_name='press_items')
    #auto_create_url = models.URLField(max_length=255, blank=True, help_text=_("Autoclipper URL. Content will be download on PUBLISH this page."))
    automated = models.BooleanField(default=False)
    title = models.CharField(max_length=255, blank=True)
    link = models.URLField(max_length=255, blank=True)
    site_name = models.CharField(max_length=255, blank=True)
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name = _("Image")
    )
    body = RichTextField(blank=True, verbose_name=_('Description'))
    panels = [
        #FieldPanel('auto_create_url'),
        FieldPanel('title'),
        FieldPanel('link'),
        ImageChooserPanel('image'),
        FieldPanel('body'),
    ]


# Download Press items section
import sys
import os
import urllib.request
import lxml.html
from django.conf import settings

from django.db.models.signals import pre_save, post_delete
from django.dispatch import receiver
from wagtail.wagtailcore.models import Collection

def create_press_item(thePage, url):
    if url:
        data = {'title':'', 'link':'', 'body':'', 'site_name':'', 'image':''}
        og = {
            "og:title":"title",
            "og:url":"link",
            "og:description":"body",
            "og:image":"image",
        }
        can_create = False
        try:
            req = urllib.request.Request(url,
                headers={
                    'User-Agent': "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.82 Safari/537.36"
                }
            )
            with urllib.request.urlopen(req) as f:
                raw = f.read()
                try:
                    data['site_name'] = req.host
                except:pass
                dom = lxml.html.document_fromstring( raw.decode('utf-8') )

                # searching in META
                r = dom.xpath('*/meta/@property')
                for e in r:
                    prop = e.getparent().attrib['property']
                    if(prop in og):
                        data[og[prop]] = e.getparent().attrib['content']

                # trying page title
                if not data['title']:
                    r = dom.xpath('*/title')
                    if len(r) > 0:
                        data['title'] = r[0].text
                        can_create = True

                # getting first image from content
                if not data['image']:
                    try:
                        r = dom.xpath('//img')
                        if len(r) > 0:
                            can_create = True
                            data['image'] = r[0].attrib['src']
                            #for e in r:
                                #print(e[0].attrib['src'])
                    except:
                        print("Exception when getting images")


                # download and create Image
                if data['image']:
                    fn = os.path.join('original_images', data['image'].split('/')[-1])
                    urllib.request.urlretrieve(data['image'], os.path.join(settings.MEDIA_ROOT, fn) )

                    try:
                        theCollection = Collection.objects.get(name = COLLECTION_NAME)
                    except Collection.DoesNotExist:
                        theCollection = Collection.get_first_root_node().add_child(instance = Collection(
                            name = COLLECTION_NAME
                        ))
                        theCollection.save()
                    
                    # path should be relative
                    image = Image(title='Press Image')
                    image.file = fn
                    image.collection_id = theCollection.id
                    image.save()
                    image.tags.add('press')
                    image.save()

                    data['image'] = image

                # saving to db
                data['automated'] = True
                thePage.press_items.add(PressItem( **data ))
                return True
        except: # catch *all* exceptions
            #e = sys.exc_info()[0]
            #print( e )
            
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print(exc_type, exc_value, exc_traceback)
            return False

        #except:
            #print(">Some Exception on press article download. URL:"+ url +"<")
            #return False


@receiver(post_delete, sender=PressItem)
def delete_press(sender, instance, *args, **kwargs):
    try:
        if instance.automated:
            instance.image.delete();
    except:
        pass


#@receiver(page_published, sender=PressPage)
@receiver(pre_save, sender=PressPage)
def download_press(sender, instance, *args, **kwargs):
    if None == kwargs['update_fields'] and instance.auto_create_urls:
        res = ''
        can_clear = False
        lst = instance.auto_create_urls.splitlines()
        for url in lst:
            url = url.strip()
            if url:
                try:
                    PressItem.objects.get(link=url, page_id=instance.id)
                    can_clear = True
                except PressItem.DoesNotExist:
                    if create_press_item(instance, url):
                        can_clear = True
                    else:
                        res += url + "\n" # not processed URL will be left in auto_create_urls fields

        if can_clear:
            if res:
                res = res.strip()
            if not res :
                instance.auto_create_urls = ''


"""
http://www.cnn.com/2016/01/22/us/winter-snowstorm-washington-blizzard/index.html
http://www.newyorker.com/magazine/2016/01/25/seeing-and-believing-the-art-world-peter-schjeldahl
http://www.huffingtonpost.com/entry/clinton-steps-up-attacks-on-sanders-days-before-iowa_us_56a168e2e4b0d8cc10997cf5
http://www.huffingtonpost.com/entry/questlove-prince_us_56a19242e4b0d8cc109992e2
http://www.huffingtonpost.com/entry/winter-storm-jonas_us_56a02406e4b076aadcc53d36
http://www.newyorker.com/books/page-turner/how-a-city-in-france-got-the-worlds-first-short-story-vending-machines
"""
