import re
import os
import datetime
import uuid

from django.template.loader import render_to_string
from datetime import date
from django import template
from django.conf import settings
from django.utils import translation
from django.utils.safestring import mark_safe

from wtsocialfeed.models import *

from django.conf import settings as system_settings

register = template.Library()

@register.inclusion_tag('wtsocialfeed/tags/social_feed.html', takes_context=True)
def social_feed(context, add_class='', rows=1, cols=0, socn=None ):
    items = SocialfeedPage.objects.all()
    if len(items):
        items = items[0]
        items = items.social_content_items.all()
        
    return {
        'add_class': add_class,
        'cols': cols,
        'rows': rows,
        'social_content_items': items.filter(socn__exact=socn) if socn else items,
        'request': context['request'],
    }
