import sys
import pytz
import traceback
from django.db import models
from django import forms

#from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel

from wagtail.wagtailimages.models import Image

from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey


#locale
from django.utils.translation import ugettext_lazy as _

# fetching feeds
from django.conf import settings
import os
from django.db import transaction
import urllib.request
import json
import tweepy
from tweepy import OAuthHandler
import datetime
from wagtail.wagtailcore.models import Collection

import textwrap

## WT
from wtpages.models import *


COLLECTION_NAME = 'Social Feed'
SOCIAL_NETWORKS = (
    ('fb', _("Facebook")),
    ('tw', _("Twitter")),
    ('in', _("Instagram")),
    ('no', _("Unlisted")),
)

class SocialNetworkItem(Orderable, models.Model):
    page = ParentalKey('wtsocialfeed.SocialfeedPage', related_name='social_items')

    socn = models.CharField(max_length=10, choices=SOCIAL_NETWORKS, verbose_name=_("Social Network") )
    title = models.CharField(max_length=255, blank=True, help_text=_("Using for filtering. If empty it showing Social Network Name in filter.") )
    link = models.URLField(max_length=255, blank=True)

    show_in_feed = models.BooleanField(default=True)
    show_in_links = models.BooleanField(default=True)
    
    instagram_token = models.ForeignKey(
        'wtinstagramtokens.WtInstagramToken',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    
    fontawesome_icon = models.CharField(max_length=50, null=True, blank=True, help_text=_("Set any icon listed there http://fontawesome.io/icons/") )

    panels = [
        FieldPanel('socn'),
        FieldPanel('link'),
        FieldPanel('title'),
        FieldPanel('show_in_feed'),
        FieldPanel('show_in_links'),
        FieldPanel('instagram_token'),
        FieldPanel('fontawesome_icon'),
        
                
    
    ]
    def social_network_name(self):
        return dict(SOCIAL_NETWORKS)[self.socn]



class SocialContentItem(models.Model):
    page = ParentalKey('wtsocialfeed.SocialfeedPage', related_name='social_content_items')
    parent_id = models.IntegerField(default=0) # network item

    socn = models.CharField(max_length=10) # name short
    account = models.CharField(max_length=40) # account id

    feed_datetime = models.DateTimeField(blank=True,null=True)
    feed_user = models.CharField(max_length=255,blank=True,null=True)
    feed_text = models.TextField(blank=True,null=True)
    feed_status = models.CharField(max_length=40,blank=True,null=True)
    #feed_image = models.CharField(max_length=255)
    feed_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        verbose_name = _("Image")
    )

    feed_thumbnail = models.CharField(max_length=255,blank=True,null=True)
    feed_dsc = models.CharField(max_length=255,blank=True,null=True)
    feed_id = models.CharField(max_length=255,blank=True,null=True)
    feed_link = models.CharField(max_length=255,blank=True,null=True)
    timezone = models.CharField(max_length=255,blank=True,null=True)

    invalid = models.BooleanField(default=True)


class SocialfeedCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('wtsocialfeed.SocialfeedPage', related_name='carousel_items')
class SocialfeedPage(WtBasePage):
    class Meta:
        verbose_name = _("Social Media Feed And Configuration")
    body = RichTextField(blank=True)

    def get_sitemap_urls(self):        
        return []

    subpage_types = []
    inline_template = 'wtsocialfeed/includes/feed.html'
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('body', classname="full "),
            FieldPanel('theme'),
        ],heading=_('Basic')),
        MultiFieldPanel([InlinePanel('social_items'),],heading=_("Social Media Feed Items"), classname="collapsible"),
        MultiFieldPanel([InlinePanel('carousel_items'),],heading=_('Images'), classname="collapsible collapsed"),
    ]
    
    def split_long_words(self, s, num):
        ret = ""
        words = s.split()
        for word in words:
            if len(word) > num:
                word = "&shy;".join(textwrap.wrap(word, num))
            ret += word + " "
        return ret         
                

    @transaction.atomic
    def save(self, *args, **kwargs):
        result = super(SocialfeedPage, self).save(*args, **kwargs)
        for soc in self.social_items.all():
            SocialContentItem.objects.filter(page_id = self.id, parent_id=soc.id).update(invalid = not soc.show_in_feed)
        return result

    def get_image(self, folder, url, title, tags, fn=None):
        if not fn:
            fn = url.split('/')[-1]
            fn = fn.split('?')[0]
        fn = os.path.join(folder, fn)
        
        try:
            
            if not os.path.exists(os.path.join(settings.MEDIA_ROOT, folder)):
                os.makedirs(os.path.join(settings.MEDIA_ROOT, folder))
            
            if not os.path.isfile( os.path.join(settings.MEDIA_ROOT, fn) ):
                urllib.request.urlretrieve(url, os.path.join(settings.MEDIA_ROOT, fn) )                
                
            try:
                image = Image.objects.get(
                    file = fn,
                    title=title,
                )
            except Image.DoesNotExist:
                try:
                    theCollection = Collection.objects.get(name = COLLECTION_NAME)
                except Collection.DoesNotExist:
                    theCollection = Collection.get_first_root_node().add_child(instance = Collection(
                        name = COLLECTION_NAME
                    ))
                    theCollection.save()
    
                image = Image(title=title)
                image.file = fn
                image.collection_id = theCollection.id
                image.save()
                if tags:
                    image.tags.add(*tags)
                    image.save()
    
            return image
                
        except:
            return None
                    


    @transaction.atomic
    def fetch_tw(self, parent, account):
        print('Fetching data from Twitter: %s' % account)        
        # delete old records     
        SocialContentItem.objects.filter(page_id = self.id, parent_id=parent.id, socn = 'tw', account=account).delete()
        
        page = self
        
        auth = OAuthHandler(settings.SOC_TW_CONSUMER_KEY, settings.SOC_TW_CONSUMER_SECRET)
        auth.set_access_token(settings.SOC_TW_USER_TOKEN, settings.SOC_TW_USER_SECRET)
        api = tweepy.API(auth)

        #http://tweepy.readthedocs.org/en/v3.2.0/api.html
        #API.user_timeline([id/user_id/screen_name][, since_id][, max_id][, count][, page])
#         maxi = SocialContentItem.objects.filter(page_id = page.id, socn = 'tw').order_by('-feed_datetime')
        
#         if len(maxi) > 0:
#             mi = maxi[0].feed_id

        mi = None
        for status in tweepy.Cursor(api.user_timeline, screen_name=account, count=self.max_feeds, since_id=mi ).items(self.max_feeds):
            #print(json.dumps(status._json, indent=4, sort_keys=True))
            
            feed_image = ''
            feed_thumbnail = ''
            
            locs = status
            try:
                if status.retweeted_status:
                    locs = status.retweeted_status
            except:
                pass
            
            try:
                                
                for row in locs.extended_entities["media"]:
                    if "photo" == row["type"]: 
                        feed_thumbnail = row["media_url"] + ':thumb'
                        feed_image = row["media_url"] 
                        break
            except:
                pass            

            folder = 'original_images/social/tw'
            image = self.get_image(folder, feed_image, "Twitter Image", None) if feed_image else None
            
            SocialContentItem(
                page_id = page.id,
                account = account,
                parent_id = parent.id,
                socn = 'tw',
                feed_datetime = datetime.datetime.strptime(status._json["created_at"], '%a %b %d %H:%M:%S %z %Y'),
                feed_user = locs.user.screen_name,
                feed_text   = locs.text,
                feed_status = 'Approved',
                feed_image = image,
                feed_thumbnail = feed_thumbnail,
                feed_dsc = '',
                feed_id = status.id_str,
                feed_link = 'https://twitter.com/statuses/' + status.id_str,
                timezone = status.user.time_zone,
                invalid = False,
            ).save(force_insert=True)

    @transaction.atomic
    def fetch_in(self, parent, account):      
        print('Fetching data from Instagram: %s' % account)
        
        # delete old records
        SocialContentItem.objects.filter(page_id = self.id, parent_id=parent.id, socn = 'in', account=account).delete()
        
        page = self      
        if parent.instagram_token and parent.instagram_token.token and parent.instagram_token.user_id:
            url = "https://api.instagram.com/v1/users/" + parent.instagram_token.user_id + "/media/recent/?access_token=" + parent.instagram_token.token +"&count="+str(self.max_feeds)
            #print(url);
            req = urllib.request.Request(url)
            data = json.loads(urllib.request.urlopen(req).read().decode('utf8'))
            for item in data["data"]:
                #print(json.dumps(item, indent=4, sort_keys=True))
                
                folder = 'original_images/social/in'
                image = self.get_image(folder, item["images"]["standard_resolution"]["url"], "Instagram Image", None)

                SocialContentItem(
                    page_id = page.id,
                    account = account,
                    parent_id = parent.id,
                    socn = 'in',
                    feed_status = 'Approved',
                    feed_dsc = '',                        
                    feed_datetime = datetime.datetime.fromtimestamp(int(item["created_time"]), pytz.UTC),
                    feed_user = item["user"]["full_name"] if item["user"] else "",
                    feed_text = item["caption"]["text"] if item["caption"] else "",
                    feed_image = image,
                    feed_thumbnail = item["images"]["thumbnail"]["url"],
                    feed_id = item['id'],
                    feed_link = item['link'],
                    timezone = '',
                    invalid = False,
                ).save(force_insert=True)

    @transaction.atomic
    def fetch_fb(self, parent, account):
        print('Fetching data from Facebook: %s' % account)
        # delete old records
        SocialContentItem.objects.filter(page_id = self.id, parent_id=parent.id, socn = 'fb', account=account).delete()
        
        page = self
        
        url = "https://graph.facebook.com/oauth/access_token?type=client_cred&client_id=" + settings.SOC_FB_APP_ID +"&client_secret=" + settings.SOC_FB_APP_SECRET
        req = urllib.request.Request(url)        
        
        data = json.loads(urllib.request.urlopen(req).read().decode('utf8'))
        access_token = "access_token="+data["access_token"]

        url = "https://graph.facebook.com/" + account + "/posts?limit=" + str(self.max_feeds) + "&"+access_token + "&fields=id,from,message,picture,link,type,object_id,created_time"
        
        req = urllib.request.Request(url)
        data = json.loads(urllib.request.urlopen(req).read().decode('utf8'))

        for item in data["data"]:            
            folder = 'original_images/social/fb'
            image = None
            picture = item.get("picture", None)  
            
            if item['type'] == 'photo':                    
                try:
                    url = "https://graph.facebook.com/"+item['object_id']+"?"+access_token + "&fields=images"
                    req = urllib.request.Request(url)
                    i_posts = json.loads(urllib.request.urlopen(req).read().decode('utf8'))
                    if len( i_posts["images"] ):
                        #print(json.dumps(i_posts["images"], indent=4, sort_keys=True))
                        cur_image = i_posts["images"][0]
                        for im in i_posts["images"]:
                            if im['width'] > cur_image['width'] :                                
                                cur_image = im;
                        picture = cur_image['source']                        
                except:
                    pass
            
            elif item['type'] == 'video':
                try:
                    if not 'object_id' in item:
                        item['object_id'] = item['link'].rstrip('/').split('/')[-1]                     
                    url = "https://graph.facebook.com/"+item['object_id']+"?"+access_token + "&fields=format"
                    req = urllib.request.Request(url)
                    i_posts = json.loads(urllib.request.urlopen(req).read().decode('utf8'))
                    if len( i_posts["format"] ):
                        cur_image = i_posts["format"][0]
                        for im in i_posts["format"]:
                            if im['width'] > cur_image['width'] :                                
                                cur_image = im;
                        picture = cur_image['picture']
                except:
                    traceback.print_exc()
                    #pass    
                
                
            if picture:
                try: 
                    #https://scontent.xx.fbcdn.net/hphotos-xfp1/v/t1.0-0/p75x225/1610083_10153258026360965_4058812127676103774_n.jpg?oh=1813cc0026bab7ea8d4debf862a3dbe3&oe=56FBA773                
                    name = picture.split('?')[0]
                    image = self.get_image(folder, picture, "Facebook Image", None, name.split('/')[-1] )
                except:
                    image = None

            SocialContentItem(
                page_id = page.id,
                account = account,
                parent_id = parent.id,
                socn = 'fb',
                feed_status = 'Approved',
                feed_dsc = '',
                feed_datetime = datetime.datetime.strptime(item["created_time"], '%Y-%m-%dT%H:%M:%S%z'),
                feed_user = item["from"]["name"],
                feed_text = self.split_long_words(item.get("message", ""), 20),
                feed_image = image,
                feed_thumbnail = '',
                feed_id = item['id'],
                feed_link = item.get("link", ""),
                timezone = '',
                invalid = False,
                ).save(force_insert=True)

    @transaction.atomic
    def fetch_all(self):     
        self.max_feeds = 40
        SocialContentItem.objects.filter(page_id = self.id).update(invalid = True)
        for soc in self.social_items.all():
            if soc.show_in_feed:
                fetch_soc = getattr(self, 'fetch_'+soc.socn, None)
                if fetch_soc:
                    account = soc.link.rstrip('/').split('/')[-1]                    
                    if account:
                        # set related feed items to valid, since we have the active account with show_in_feed=True
                        # will delete them and add new ones in the fetch_soc() 
                        SocialContentItem.objects.filter(page_id = self.id, parent_id=soc.id, account=account).update(invalid = False)
                        try:
                            fetch_soc(soc, account)
                        except:
                            traceback.print_exc()

        SocialContentItem.objects.filter(page_id=self.id, invalid=True).delete()
        
    @transaction.atomic
    def clear_all(self):        
        SocialContentItem.objects.all().delete()        


