from django.core.management.base import BaseCommand, CommandError
from wtsocialfeed.models import *

class Command(BaseCommand):
    def handle(self, *args, **options):
        for item in SocialfeedPage.objects.all():
            item.clear_all()
