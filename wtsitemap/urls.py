from django.conf.urls import url
from .views import sitemap, robots

urlpatterns = [
    url(r'^sitemap\.xml$', sitemap),
    url(r'^robots\.txt$', robots),    
]
