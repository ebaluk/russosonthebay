# -*- coding: utf-8 -*-
import datetime
from django.db import models
from django import forms

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel


from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtpages.models import CarouselItem, RelatedLink
from wtblocks.models import ContentBlockThemed
from wtpages.models import *

from django.db.models import Q
import calendar

class EventIndexPage(StandardIndexPage):
    class Meta:
        verbose_name = _("Events Index Page")
    subpage_types = ['EventPage', 'wtpages.URLPage', 'wtpages.StandardPage', 'wtforms.FormLinkPage']
    template = 'wtevents/event_index_page.html'    
    


class EventPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('EventPage', related_name='carousel_items')
    
class EventPageSideCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('EventPage', related_name='side_carousel_items')
    
class EventPageBottomCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('EventPage', related_name='bottom_carousel_items')    
        
class EventPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('EventPage', related_name='related_links')
class EventPageFormLink(Orderable, WtFormLinkField):
    page = ParentalKey('EventPage', related_name='form_links')


class EventPage(WtBasePage, LayoutablePage):
    class Meta:
        verbose_name = _("Event Page")
        
    parent_page_types = ['EventIndexPage']
    subpage_types = []    
    
    template = 'wtevents/event_page.html'
    inline_template = 'wtevents/event_page_inline.html'
    
    display_date = models.CharField(max_length=255, verbose_name=_("Display Date/Time"), blank=True, null=True)
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    body = RichTextField(blank=True, verbose_name = _("Text"))
    excerpt = RichTextField(blank=True, null=True)
    
    hide_title = models.BooleanField(default=False)
    show_index_title = models.BooleanField(default=False)    
    
    carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True, null=True)
    side_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    bottom_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    
    @property
    def navbar_items(self):
        return self.get_parent().get_children().live().in_menu()
    
    @property
    def top_page_title(self):        
        return self.get_parent().title
    
    @property
    def top_page(self):
        return self.get_parent()
    
    
    @property
    def thumbnail(self):
        ret = self.side_carousel_items.filter(visible=True).first()
        if not ret:
            ret = self.carousel_items.filter(visible=True).first()
        if not ret:
            ret = self.bottom_carousel_items.filter(visible=True).first()    
        
        return ret.image if ret else None   
        
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            #FieldPanel('page_heading', classname="full title"),
            FieldPanel('display_date'),            
            FieldPanel('page_layout'),
            
            FieldPanel('excerpt'),
            FieldPanel('body'),            
            #FieldPanel('show_index_title'),
            #FieldPanel('hide_title'),
            SnippetChooserPanel('theme'),
        ],heading=_('Basic')),
        MultiFieldPanel([
            FieldPanel('carousel_style'),
            InlinePanel('carousel_items'),],heading=_('Top Images'),classname="collapsible collapsed"),
        MultiFieldPanel([
            FieldPanel('side_carousel_style'),
            InlinePanel('side_carousel_items'),],heading=_('Side Images'),classname="collapsible collapsed"),        
        MultiFieldPanel([
            FieldPanel('bottom_carousel_style'),
            InlinePanel('bottom_carousel_items'),],heading=_('Bottom Images'),classname="collapsible collapsed"),              
        MultiFieldPanel([InlinePanel('related_links'),], heading=_('Related Links'), classname="collapsible collapsed"),
        MultiFieldPanel([InlinePanel('form_links'),], heading=_('Forms'), classname="collapsible collapsed"),
    ]
    
    promote_panels = [
        MultiFieldPanel([            
            FieldPanel('slug'),
            FieldPanel('show_in_menus'),
            FieldPanel('seo_title'),
            FieldPanel('search_description'),
            FieldPanel('seo_keywords'),
        ], _('Common page configuration')),
    ]



class EventLinksPage(WtBasePage):
    class Meta:
        verbose_name = _('Event Thumbnails Page ( for Special Events, Meetings, etc. )')
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")    
    body = RichTextField(blank=True, help_text="Description")
    subpage_types = []
    template = 'wtevents/event_links_page.html'
    inline_template = 'wtevents/event_links_page_inline.html'
        
    
    @property
    def events(self):
        return EventPage.objects.live()
            
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title", ),    
            FieldPanel('page_heading', classname="full title", ),
            #FieldPanel('page_content_heading', classname="full title"),
            FieldPanel('body'),                
        ],heading=_('Basic')),
                              
    ]
    


