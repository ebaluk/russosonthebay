from wagtail.wagtailcore import blocks
from wagtail.wagtailsnippets.blocks import SnippetChooserBlock

class PrivateEventsParagraphBlock(blocks.StructBlock):
    text = blocks.RichTextBlock(required=False)    
    #font_awesome_icon = blocks.CharBlock(required=False)
    theme = SnippetChooserBlock(required=False, target_model='wtpages.CssTheme')
    class Meta:
        icon = 'form'
        template = 'wtprivateevents/blocks/paragraph.html'        

