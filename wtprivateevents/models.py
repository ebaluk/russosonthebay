from django.db import models
from django import forms

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailcore import blocks
from wagtail.wagtailsnippets.blocks import SnippetChooserBlock
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel
from wagtail.wagtailimages.blocks import ImageChooserBlock

from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtpages.models import CarouselItem, RelatedLink
from wtblocks.models import ContentBlockThemed
from wtpages.models import *
from wtprivateevents.blocks import PrivateEventsParagraphBlock


# class LayoutChoiceBlock(blocks.ChoiceBlock):
#     choices = [
#         ('left',        _("Left Text Right Image")),
#         ('right',       _("Right Text Left Image")),
#         ('whole',       _("Only Text")),
#     ]
#     class Meta:
#         icon = 'cup'
       
        
# class AboutImageBlock(ImageChooserBlock):
#     class Meta:
#         icon = 'image'
#         template = 'wtabout/blocks/image.html'        
#         

class PrivateEventsPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('PrivateEventsIndexPage', related_name='carousel_items')
     
# class MenuIndexPageRelatedLink(Orderable, RelatedLink):
#     page = ParentalKey('wtmenus.MenuIndexPage', related_name='related_links')

class StandardIndexPageSideCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('PrivateEventsIndexPage', related_name='side_carousel_items')

class PrivateEventsPageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('PrivateEventsIndexPage', related_name='related_links')

class PrivateEventsIndexPage(WtBaseIndexPage, LayoutablePage):
    carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)    
    body = StreamField([
        #('heading', blocks.CharBlock(classname="full title")),
        ('paragraph', PrivateEventsParagraphBlock()),        
    ])
    
    #hide_title = models.BooleanField(default=False)
    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    
    side_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)

    template = 'wtprivateevents/privateevents_index_page.html'      


    @property
    def navbar_items(self):
        return self.get_children().live()
    
    @property
    def top_page(self):
        return self

    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('page_heading', classname="full title"),
            FieldPanel('page_content_heading', classname="full title"),
            FieldPanel('page_layout'),
            StreamFieldPanel('body'),
            FieldPanel('theme'),
        ],heading=_('Basic')),
        MultiFieldPanel([
            FieldPanel('carousel_style'),
            InlinePanel('carousel_items'),],heading=_('Top Images'),classname="collapsible collapsed"),
        MultiFieldPanel([
            FieldPanel('side_carousel_style'),
            InlinePanel('side_carousel_items'),],heading=_('Side Images'),classname="collapsible collapsed"),              
        MultiFieldPanel([InlinePanel('related_links'),],heading=_("Related links"),classname="collapsible collapsed"),
    ]

   
