# -*- coding: utf-8 -*-
from django.db import models
from django import forms

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel

from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

from modelcluster.models import ClusterableModel

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtblocks.models import ContentBlockThemed
from wtpages.models import *

class VenueLinksPagelItem(Orderable):
    page = ParentalKey('VenueLinksPage', related_name='venue_items')
    venue = models.ForeignKey(
        'VenuePage',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='+',
        #help_text="Special Events, Meetings, etc. Sub-Page",   
    )

class VenueLinksPage(WtBasePage):
    class Meta:
        verbose_name = _('Venue Thumbnails Page ( for Special Events, Meetings, etc. )')
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")    
    body = RichTextField(blank=True, help_text="Description")
    subpage_types = []
    template = 'wtvenues/venue_links_page.html'
    inline_template = 'wtvenues/venue_links_page_inline.html'
        
    
    @property
    def venues(self):
        return self.venue_items.filter(venue__live=True)
            
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title", ),    
            FieldPanel('page_heading', classname="full title", ),
            FieldPanel('page_content_heading', classname="full title"),
            FieldPanel('body'),                
        ],heading=_('Basic')),
        MultiFieldPanel([
            InlinePanel('venue_items'),],heading=_('Available Venues'),classname="collapsible"),                      
    ]
    

class VenueIndexPageCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('VenueIndexPage', related_name='carousel_items')

class VenueIndexPage(WtBasePage):
    class Meta:
        verbose_name = _("Venues Index Page (Main Venues Page)")
        
    subpage_types = ['VenuePage', 'wtpages.URLPage', 'wtpages.StandardPage', 'wtforms.FormLinkPage']

    page_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    body = RichTextField(blank=True, verbose_name = _("Text"))
    carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    

    @property
    def venues(self):
        return VenuePage.objects.live()

    @property
    def navbar_items(self):        
        return self.get_children().live().in_menu()
    
    @property
    def top_page_title(self):        
        return self.title
    
    @property
    def top_page(self):
        return self

    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('page_heading', classname="full title"),
            FieldPanel('body'),                
        ],heading=_('Basic')),
        MultiFieldPanel([
            FieldPanel('carousel_style'),
            InlinePanel('carousel_items'),],heading=_('Top Images'),classname="collapsible collapsed"),                      
    ]


class VenuePageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('VenuePage', related_name='carousel_items')
class VenuePageSideCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('VenuePage', related_name='side_carousel_items')    
class VenuePageBottomCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('VenuePage', related_name='bottom_carousel_items')    
class VenuePageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('VenuePage', related_name='related_links')
class VenuePageFormLink(Orderable, WtFormLinkField):
    page = ParentalKey('VenuePage', related_name='form_links')
    

class VenuePage(WtBasePage, LayoutablePage):
    template = 'wtvenues/venue_page.html'    
    class Meta:
        verbose_name = _('Venue Page')
        
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    body = RichTextField(blank=True, verbose_name = _('Overview'))        
    carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    side_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    bottom_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    
    template = 'wtpages/standard_page.html'
    
    subpages_inline = True
    
    parent_page_types = ['VenueIndexPage']
    subpage_types = ['wtgallery.GalleryCollectionPage', 'wtpages.URLPage', 'wtforms.FormLinkPage']
    
    @property
    def navbar_items(self):        
        return self.get_children().live().in_menu()
    
    content_panels = [
        MultiFieldPanel(
            [
                FieldPanel('title', classname="full title"),
                FieldPanel('page_heading', classname="full title"),
                FieldPanel('page_layout'),                
                FieldPanel('body', ),                
            ],
            heading="Basic",
        ),
        MultiFieldPanel([
            FieldPanel('carousel_style'),
            InlinePanel('carousel_items'),],heading=_('Top Images'),classname="collapsible collapsed"),
        MultiFieldPanel([
            FieldPanel('side_carousel_style'),
            InlinePanel('side_carousel_items'),],heading=_('Side Images'),classname="collapsible collapsed"),              
        MultiFieldPanel([
            FieldPanel('bottom_carousel_style'),
            InlinePanel('bottom_carousel_items'),],heading=_('Bottom Images'),classname="collapsible collapsed"),              
        MultiFieldPanel([InlinePanel('related_links'),],heading=_('Related Links'),classname="collapsible collapsed"),
        MultiFieldPanel([InlinePanel('form_links'),],heading=_('Forms'),classname="collapsible collapsed"),                      
        
    ]


