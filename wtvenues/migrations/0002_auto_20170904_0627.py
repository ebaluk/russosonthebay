# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-04 06:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wtvenues', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='venuelinkspage',
            options={'verbose_name': 'Venue Thumbnails Page ( for Special Events, Meetings, etc. )'},
        ),
        migrations.AddField(
            model_name='venuelinkspage',
            name='page_heading',
            field=models.CharField(blank=True, help_text='The page content heading', max_length=255),
        ),
    ]
