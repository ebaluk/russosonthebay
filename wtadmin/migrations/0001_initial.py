# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-05-10 11:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('wagtailcore', '0032_add_bulk_delete_page_permission'),
        ('wagtailimages', '0017_reduce_focal_point_key_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='WtSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('caption', models.CharField(max_length=255)),
                ('smtp_host', models.CharField(blank=True, max_length=255, null=True)),
                ('smtp_user', models.CharField(blank=True, max_length=255, null=True)),
                ('smtp_pass', models.CharField(blank=True, max_length=255, null=True)),
                ('reservations_box_text', wagtail.wagtailcore.fields.RichTextField(blank=True, verbose_name='Reservations Box Text')),
                ('logo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.Image')),
                ('site', models.OneToOneField(editable=False, on_delete=django.db.models.deletion.CASCADE, to='wagtailcore.Site')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
