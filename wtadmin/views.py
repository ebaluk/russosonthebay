from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.cache import never_cache, cache_page
from collections import OrderedDict
from wagtail.wagtailimages.models import Image


#~ class DisableCSRF(object):
    #~ def process_request(self, request):
        #~ setattr(request, '_dont_enforce_csrf_checks', True)


@never_cache
def wtadmin_delete_images(request):
    if not request.user.has_perm('wagtailadmin.access_admin'):
        return JsonResponse({})
    data = {}
    if request.POST:
        req = dict(request.POST)
        if 'images[]' in req:
            for img in req['images[]']:
                image_id = img.split('/')[3]

                image = Image.objects.filter(id = image_id)
                if len(image):
                    try:
                        image.delete()
                    except:
                        pass

            data['code'] = 'location.reload();'
    return JsonResponse(data)
