from django import forms
from django.db import models

from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel

#from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
#
#from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

#locale
from django.utils.translation import ugettext_lazy as _

@register_setting
class WtSettings(BaseSetting):

    caption = models.CharField(max_length=255)
    logo = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    smtp_host = models.CharField(max_length=255, null=True,blank=True,)
    smtp_user = models.CharField(max_length=255, null=True,blank=True,)
    smtp_pass = models.CharField(max_length=255, null=True,blank=True,)
    
    reservations_box_text = RichTextField(blank=True, verbose_name = _("Reservations Box Text"))
    
    
    page_404_text = RichTextField(blank=True, verbose_name = _("Page Not Found Text"))
    page_404_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    
    
    @property
    def page_404_images(self):
        return [{'image': self.page_404_image, 'visible': True,},] if self.page_404_image else None  

    panels = [
        MultiFieldPanel([
                FieldPanel('caption'),
                ImageChooserPanel('logo'),
            ],
            heading=_('Admin Branding'),
            #classname="collapsible collapsed"
        ),
        MultiFieldPanel([
                FieldPanel('smtp_host'),
                FieldPanel('smtp_user'),
                FieldPanel('smtp_pass'),
            ],
            heading=_('Email settings'),
            #classname="collapsible collapsed"
        ),
              
        #FieldPanel('reservations_box_text'),
        FieldPanel('page_404_text'),
        ImageChooserPanel('page_404_image'),
        
              
              
    ]
