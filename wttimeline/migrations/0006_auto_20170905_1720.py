# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-05 17:20
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0040_page_draft_title'),
        ('wagtailimages', '0019_delete_filter'),
        ('wagtaildocs', '0007_merge'),
        ('wttimeline', '0005_auto_20170905_1649'),
    ]

    operations = [
        migrations.CreateModel(
            name='TimelinePageBottomCarouselItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
                ('link_external', models.URLField(blank=True, verbose_name='External link')),
                ('embed_url', models.URLField(blank=True, verbose_name='Embed URL')),
                ('caption', models.TextField(blank=True)),
                ('text', wagtail.wagtailcore.fields.RichTextField(blank=True)),
                ('visible', models.BooleanField(default=1)),
                ('image', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.Image')),
                ('link_document', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtaildocs.Document')),
                ('link_page', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtailcore.Page')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='timelinepage',
            name='bottom_carousel_style',
            field=models.CharField(blank=True, choices=[('carousel', 'Carousel'), ('fading', 'Fading')], default='carousel', max_length=255),
        ),
        migrations.AddField(
            model_name='timelinepagebottomcarouselitem',
            name='page',
            field=modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='bottom_carousel_items', to='wttimeline.TimelinePage'),
        ),
    ]
