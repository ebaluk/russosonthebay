from django.db import models
from django import forms

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel

from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

from wagtail.wagtailimages.models import *

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtblocks.models import ContentBlockThemed
from wtpages.models import *

class TimelinePagePageCarouselItem(Orderable):
    page = ParentalKey('TimelinePage', related_name='carousel_items')    
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )    
    year = models.CharField(max_length=10, )
    title = models.TextField(max_length=255, blank=True, null=True,)
    text = RichTextField(blank=True)
    visible = models.BooleanField(blank=True, default=1)
    
    @property
    def caption(self):
        return "%s - %s" % (self.year, self.title)    
    
    panels = [
        ImageChooserPanel('image'),
        #FieldPanel('embed_url'),
        FieldPanel('year'),
        FieldPanel('title'),
        FieldPanel('text'),
        #MultiFieldPanel(LinkFields.panels, "Link"),
        FieldPanel('visible'),
    ]

  
class TimelinePageBottomCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('TimelinePage', related_name='bottom_carousel_items')

class TimelinePage(WtBasePage):
    
    class Meta:
        verbose_name = _('Timeline Page')
    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    #body = RichTextField(blank=True)
    
    bottom_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    template = 'wttimeline/timeline_page.html'    
    inline_template = 'wttimeline/timeline_page_inline.html'
    
    #carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    #parent_page_types = ['wthomepage.HomePage']
    subpage_types = []
    
    subpages_inline = False
    page_layout = 'whole'
    
    @property
    def items(self):
        return self.carousel_items.filter(visible=True)        
        
    
    @property
    def odd_items(self):
        items = list(self.carousel_items.filter(visible=True))        
        return [{'item': items[i], 'num': i} for i in range(0, len(items), 2)] if items else None 
    
    @property
    def even_items(self):
        items = list(self.carousel_items.filter(visible=True))        
        return [{'item': items[i], 'num': i} for i in range(1, len(items), 2)] if len(items) > 1 else None
    
    content_panels = [
        MultiFieldPanel(
            [
                FieldPanel('title', classname="full title"),
                FieldPanel('page_heading', classname="full title"),
                FieldPanel('page_content_heading', classname="full title"),
                #FieldPanel('body', ),
            ],
            heading="Basic",
        ),
        MultiFieldPanel([
            #FieldPanel('carousel_style'),
            InlinePanel('carousel_items'),],heading=_('Timeline Items'),classname="collapsible collapsed"),
        
        MultiFieldPanel([
            FieldPanel('bottom_carousel_style'),
            InlinePanel('bottom_carousel_items'),],heading=_('Bottom Images'),classname="collapsible collapsed"),              
                                    
       #/MultiFieldPanel([InlinePanel('carousel_items'),],heading=_('Images'), classname="collapsible collapsed"),
        
    ]


