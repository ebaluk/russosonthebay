
$(function(){
	
	var timer;
	var $carousel = $('#carousel-timeline');
	var $time_line_nav = $('section.time-line-nav');
	var $table = $('>div', $time_line_nav);
		
	var ajustTimeLineNav = function(){
		var ww = $(window).width();
		var ww2 = ww/2;
		var nw = $table.width();
		
		var dx = 0;
		var $td = $('td.year.active');
		var tdLeft = 0;
		if($td.length){
			var p = $td.position();
			tdLeft = p.left + $td.outerWidth();
		}
		
		if( tdLeft > ww2 ){
			var rw = nw - tdLeft;						
			dx = ww2 - tdLeft;			
			if( rw < ww2 ){
				dx = dx + (ww2 - rw); 	 
			}
		}
		$table.css({left: dx + 'px'});
	}; 
	
	$window.on('resize', function(){
		clearTimeout(timer);
		timer = setTimeout(function(){ajustTimeLineNav();}, 400);
    });
     
    
    $carousel.on('slide.bs.carousel', function (e) {    	
    	var activeIdx = $('.carousel-inner>.item', $carousel).index($(e.relatedTarget));
    	var $td = $('td[data-slide-to="'+activeIdx+'"]');    	
    	$('td[data-slide-to]').removeClass('active');
    	$td.addClass('active');    	
    	ajustTimeLineNav();
    });
    
    
    $('td[data-slide-to]').on('mouseenter', function(){
    	var $this = $(this);
    	var num = $this.data('slide-to');    	
    	var $tdnum = $('td[data-slide-to="'+num+'"]');
    	$tdnum.addClass('hover');
    }).on('mouseleave', function(){
    	$('td[data-slide-to]').removeClass('hover');
    });
    
    
});    