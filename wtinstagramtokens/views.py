import requests
from django import http
from wtinstagramtokens.models import WtInstagramToken

def instagram_config (request):
    code = request.GET.get('code', '')
    pk = int(request.GET.get('pk', '0'))    
    admin_url = "/admin/wtinstagramtokens/wtinstagramtoken/edit/%s/" % pk
    
    if code:
        obj = WtInstagramToken.objects.get(id=pk)
        try:
            redirect_url = "http://%s/instagram_config/?pk=%d" % (request.META['HTTP_HOST'], pk);
            data = {
                "client_id": obj.client_id,
                "client_secret": obj.client_secret,
                "redirect_uri": redirect_url,
                "code": code,
                "grant_type": "authorization_code",            
                }
        
            url = "https://api.instagram.com/oauth/access_token"
            req = requests.post(url, data=data)
            status = req.status_code
            json = req.json()
        
            if 200 == status and json["access_token"]:
                obj.status = "success"
                obj.client_id = ''
                obj.client_secret = ''
                obj.token = json["access_token"]
                obj.user_id = json["user"]["id"]
                obj.user_name = json["user"]["full_name"]
                obj.profile_photo = json["user"]["profile_picture"]
        except:
            obj.status = "error"
        
        obj.save()        
    
    return http.HttpResponsePermanentRedirect(admin_url)      
