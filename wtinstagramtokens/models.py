from django import forms
from django.db import models
from django.conf import settings
from django.utils.safestring import mark_safe
from django.http import HttpRequest  

from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel

#locale
from django.utils.translation import ugettext_lazy as _

from .widgets import WtInstagramTokenProfilePhotoWidget

class WtInstagramToken(models.Model):
    class Meta:
        verbose_name = _("Instagram Token")        
        
    name = models.CharField(max_length=255, null=False,blank=False, help_text='You will be able to select this Token for the Social Media Page Items')
    client_id = models.CharField(max_length=255, null=True,blank=True,)
    client_secret = models.CharField(max_length=255, null=True,blank=True, )
    
    token = models.CharField(max_length=255, null=True,blank=True,)
    user_id = models.CharField(max_length=255, null=True,blank=True,)
    user_name = models.CharField(max_length=255, null=True,blank=True,)
    profile_photo = models.CharField(max_length=255, null=True,blank=True,)
    
    access_url = models.CharField(max_length=255, null=True,blank=True,)    
    status = models.CharField(max_length=255, null=True,blank=True,)
    
    process=True
    
    def __str__(self):
        return self.name
    
    #FieldPanel('title_lat',widget=forms.TextInput(attrs={'readonly':'readonly'})),#,'disabled':'disabled'
    panels = [
        MultiFieldPanel([
                FieldPanel('name', classname="full title"),                
            ],
            heading=_('Common'),          
        ),

        MultiFieldPanel([                
                FieldPanel('client_id'),
                FieldPanel('client_secret'),
            ],
            heading=mark_safe(_('Instagram settings <span style="text-transform: none;">(please fill them out and click the save button to obtain the Instagram Token.)</span>')),          
        ),              
              
        MultiFieldPanel([
                FieldPanel('token', widget=forms.TextInput(attrs={'readonly':'readonly'})),
                FieldPanel('user_id', widget=forms.TextInput(attrs={'readonly':'readonly'})),
                FieldPanel('user_name', widget=forms.TextInput(attrs={'readonly':'readonly'})),
                FieldPanel('profile_photo', widget=WtInstagramTokenProfilePhotoWidget ),            
                FieldPanel('status', widget=forms.TextInput(attrs={'readonly':'readonly'})),
            ],
            heading=_('Instagram Token And Status (read only)'),          
        ),
    ]

