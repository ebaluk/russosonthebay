from django.apps import AppConfig

class InstagramTokensConfig(AppConfig):
    name = 'instagramtokens'
    verbose_name = 'Instagram Tokens'