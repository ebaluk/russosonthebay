from django.shortcuts import redirect
from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register
from wagtail.contrib.modeladmin.views import CreateView, EditView
from wagtail.wagtailadmin import messages
from urllib.parse import urlencode
from django.utils.safestring import mark_safe  
from .models import WtInstagramToken


 
class WtInstagramTokenMixin(object):
    
    def form_valid(self, form):
        instance = form.save()
        messages.success(
            self.request, self.get_success_message(instance),
            buttons=self.get_success_message_buttons(instance)
        )
        
        if instance.client_id and instance.client_secret:
            instance.status = "awaiting"
            instance.save()            
            access_url = "https://api.instagram.com/oauth/authorize/?" +urlencode({
                    "client_id": instance.client_id,
                    "response_type": "code",
                    "redirect_uri": "http://%s/instagram_config/?pk=%d" % (self.request.META['HTTP_HOST'], instance.id)
                })
            return redirect(access_url)
        
        return redirect(self.get_success_url())
    
class WtInstagramTokenEditView(WtInstagramTokenMixin, EditView):
    pass

class WtInstagramTokenCreateView(WtInstagramTokenMixin, CreateView):
    pass


class WtInstagramTokenAdmin(ModelAdmin):
    form_view_extra_css = ['wtinstagramtokens/style.css']
    create_view_class = WtInstagramTokenCreateView
    edit_view_class = WtInstagramTokenEditView
    model = WtInstagramToken    
    menu_icon = 'doc-full-inverse'
    #menu_icon = 'date'
    list_display = ('name', 'status', )
    #list_filter = ['date']
    #search_fields = ('title')
    menu_order = 110 
    add_to_settings_menu = True
    

modeladmin_register(WtInstagramTokenAdmin)


# WtInstagramToken.client_id.help_text = mark_safe('<ol><li>Login to the Instagram.</li>'
#             '<li>Open the https://www.instagram.com/developer/clients/manage/ and register new client.</li>'
#             '<li>Fill out the fields. Click on the Security tab and add the http://%s/instagram_config/ there, please make sure there are no spaces there.</li>'
#             '<li>Save the client and open it again and copy Client ID and Secret.</li>'
#             '<li>Open the Admin/Settings/Instagram Settings and set the Client ID and Client Secret there. Please make sure there are no any spaces there.</li>'
#             '<li>Click the Save button and wait for the Instagram security settings dialog for this App. You MUST confirm access to your Instagram account.</li>'
#             '<li>When wait for a little bit and you will see the "Success" status there.</li></ol>'
#         ) % HttpRequest.META['HTTP_HOST']