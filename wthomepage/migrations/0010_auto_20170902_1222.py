# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-02 12:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wthomepage', '0009_auto_20170829_1104'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='box1_color',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='homepage',
            name='box2_color',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='homepage',
            name='box3_color',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='homepage',
            name='box4_color',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
    ]
