import datetime
import calendar
from django.db.models import Q

from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.contrib.wagtailroutablepage.models import RoutablePageMixin, route

from modelcluster.fields import ParentalKey

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtpages.models import CarouselItem, RelatedLink, LayoutablePage, SLIDESHOW_STYLE_CHOICES

from wtpages.models import *


class HomePagePlaceholder(WtBasePage):
    class Meta:
        verbose_name = _("Home Page Inline Pages")
    subpages_inline = True    
    
    def get_sitemap_urls(self):
        return []
    
    
class HomePageCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('HomePage', related_name='carousel_items')
class HomePageSideCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('HomePage', related_name='side_carousel_items')    
class HomePageBottomCarouselItem(Orderable,  CarouselItem):
    page = ParentalKey('HomePage', related_name='bottom_carousel_items')    
class HomePageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('HomePage', related_name='related_links')

class HomePageFormLink(Orderable, WtFormLinkField):
    page = ParentalKey('HomePage', related_name='form_links')
    
    
class HomePage(WtBasePage):
    body = RichTextField(blank=True, verbose_name = _("Text"))
    carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    hide_title = models.BooleanField(default=False)
    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    
    
    side_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    bottom_carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
        
    @property
    def home_placeholder(self):
        return HomePagePlaceholder.objects.live().first()
    
    @property
    def navbar_items(self):        
        return None
    
    @property
    def top_page_title(self):        
        return self.title
    
    @property
    def top_page(self):
        return self

    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            #FieldPanel('page_heading', classname="full title"),
            #FieldPanel('page_content_heading', classname="full title"),            
            FieldPanel('body'),
            #FieldPanel('page_layout'),
            #FieldPanel('hide_title'),
            #FieldPanel('theme'),
            SnippetChooserPanel('theme'),
        ],heading=_('Basic')),
        MultiFieldPanel([
            FieldPanel('carousel_style'),
            InlinePanel('carousel_items'),],heading=_('Top Images'),classname="collapsible collapsed"),
#         MultiFieldPanel([
#             FieldPanel('side_carousel_style'),
#             InlinePanel('side_carousel_items'),],heading=_('Side Images'),classname="collapsible collapsed"),              
#         MultiFieldPanel([
#             FieldPanel('bottom_carousel_style'),
#             InlinePanel('bottom_carousel_items'),],heading=_('Bottom Images'),classname="collapsible collapsed"),              
        MultiFieldPanel([InlinePanel('related_links'),],heading=_('Related Links'),classname="collapsible collapsed"),
        MultiFieldPanel([InlinePanel('form_links'),],heading=_('Forms'),classname="collapsible collapsed"),
    ]


    class Meta:
        verbose_name = _("Home Page")





