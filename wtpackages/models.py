from django.db import models
from django.utils.translation import ugettext_lazy as _
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel

from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailcore.models import Orderable
from modelcluster.fields import ParentalKey

from wtpages.models import *

class Package(models.Model):
    class Meta:
        verbose_name = _('Promo & Package')
        verbose_name_plural = _('Promo & Packages')
    
    title = models.CharField(max_length=255)
    text = RichTextField(blank=True)    
    image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )    
#     link_document = models.ForeignKey(
#         'wagtaildocs.Document',
#         null=True,
#         blank=True,
#         related_name='+'
#     )
    visible = models.BooleanField(blank=True, default=1)    
    panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title"),
            FieldPanel('text' ),
            ImageChooserPanel('image'),
            #DocumentChooserPanel('link_document'),
        ],heading="Basic",),
                        
    ]
        
    def __str__(self):
        return self.title
    


class PackageLinksPagelItem(Orderable):
    page = ParentalKey('PackageLinksPage', related_name='package_items')
    package = models.ForeignKey(
        'Package',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='+',
        #help_text="Special Events, Meetings, etc. Sub-Page",   
    )

PACKAGES_VIEW_STYLE_CHOICES = (
    ('thumbnails', _('Thumbnails')),
    ('preview', _('Image and Text')),
)

class PackageLinksPage(WtBasePage):
    class Meta:
        verbose_name = _('Promo & Package Thumbnails Page ( for Special Events, Meetings, etc. )')

    view_style = models.CharField(max_length=255, default='thumbnails', choices=PACKAGES_VIEW_STYLE_CHOICES, blank=True)    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")    
    body = RichTextField(blank=True, help_text="Description")
    subpage_types = []
    template = 'wtpackages/package_links_page.html'
    inline_template = 'wtpackages/package_links_page_inline.html'
    
    page_layout = 'whole'
        
    @property
    def packages(self):
        return self.package_items.filter(package__visible=True)    
        
    content_panels = [
        MultiFieldPanel([
            FieldPanel('title', classname="full title", ),
            FieldPanel('page_heading', classname="full title", ),
            FieldPanel('page_content_heading', classname="full title"),
            FieldPanel('view_style'),
            FieldPanel('body'),                
        ],heading=_('Basic')),
        MultiFieldPanel([
            InlinePanel('package_items'),],heading=_('Available Packages'),classname="collapsible"),                      
    ]
    

