from wagtail.contrib.modeladmin.options import ModelAdmin,  modeladmin_register

from .models import Package

class PackageAdmin(ModelAdmin):
    model = Package
    menu_label = 'Promo & Packages'
    menu_icon = 'doc-full'
    list_display = ('title',  )
    search_fields = ('title')
    menu_order = 430
    add_to_settings_menu = False

modeladmin_register(PackageAdmin)
