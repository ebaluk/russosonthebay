# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-02 15:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields
import wagtail.wagtailcore.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wtpages', '0005_auto_20170902_1500'),
        ('wagtailcore', '0040_page_draft_title'),
        ('wtpackages', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PackageLinksPage',
            fields=[
                ('page_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='wagtailcore.Page')),
                ('seo_keywords', models.TextField(blank=True, verbose_name='SEO keywords')),
                ('body', wagtail.wagtailcore.fields.RichTextField(blank=True, help_text='Description')),
                ('theme', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wtpages.CssTheme', verbose_name='Theme')),
            ],
            options={
                'verbose_name': 'Promo & Package Thumbnails Page ( for Special Events, Meetings, etc. )',
            },
            bases=('wagtailcore.page',),
        ),
        migrations.CreateModel(
            name='PackageLinksPagelItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, editable=False, null=True)),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
        ),
        migrations.AlterModelOptions(
            name='package',
            options={'verbose_name': 'Promo & Package', 'verbose_name_plural': 'Promo & Packages'},
        ),
        migrations.AddField(
            model_name='packagelinkspagelitem',
            name='package',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wtpackages.Package'),
        ),
        migrations.AddField(
            model_name='packagelinkspagelitem',
            name='page',
            field=modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='package_items', to='wtpackages.PackageLinksPage'),
        ),
    ]
