# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-06 09:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wtpackages', '0005_auto_20170905_1122'),
    ]

    operations = [
        migrations.AddField(
            model_name='packagelinkspage',
            name='show_separator',
            field=models.BooleanField(default=False),
        ),
    ]
