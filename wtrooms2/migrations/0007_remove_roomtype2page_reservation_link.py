# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-03 04:59
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wtrooms2', '0006_roomtype2pagerelatedlink'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='roomtype2page',
            name='reservation_link',
        ),
    ]
