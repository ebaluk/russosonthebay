# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-08-30 05:17
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailcore', '0040_page_draft_title'),
        ('wtrooms2', '0002_auto_20170829_1104'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='roomtype2carouselitem',
            name='image',
        ),
        migrations.AddField(
            model_name='roomtype2carouselitem',
            name='collection',
            field=models.ForeignKey(blank=True, help_text='The Collection to take Images from', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='wagtailcore.Collection'),
        ),
        migrations.AlterField(
            model_name='roomtype2carouselitem',
            name='page',
            field=modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='collection_items', to='wtrooms2.RoomType2Page'),
        ),
    ]
