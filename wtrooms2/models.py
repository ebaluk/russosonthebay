# -*- coding: utf-8 -*-
from django.db import models
from django import forms

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField, StreamField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, InlinePanel, StreamFieldPanel, PageChooserPanel

from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtaildocs.edit_handlers import DocumentChooserPanel

from modelcluster.fields import ParentalKey

from wagtail.wagtailsearch import index
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

from wagtail.wagtailimages.models import *

#locale
from django.utils.translation import ugettext_lazy as _

## WT
from wtblocks.models import ContentBlockThemed
from wtpages.models import *

class RoomType2IndexPageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('wtrooms2.RoomType2IndexPage', related_name='carousel_items')

class RoomType2IndexPage(WtBasePage):
    
    class Meta:
        verbose_name = _('Rooms Index Page')
    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    body = RichTextField(blank=True)
    
    template = 'wtpages/standard_page.html'    
    
    carousel_style = models.CharField(max_length=255, default='carousel', choices=SLIDESHOW_STYLE_CHOICES, blank=True)
    
    parent_page_types = ['wthomepage.HomePage']
    subpage_types = ['wtrooms2.RoomType2Page']
    
    subpages_inline = True
    page_layout = 'whole'
    
    @property
    def rooms(self):
        rooms = RoomType2Page.objects.live().descendant_of(self)
        return rooms
    
    
    @property
    def navbar_items(self):
        return RoomType2Page.objects.live().in_menu().descendant_of(self)
    

    content_panels = [
        MultiFieldPanel(
            [
                FieldPanel('title', classname="full title"),
                FieldPanel('page_heading', classname="full title"),
                FieldPanel('page_content_heading', classname="full title"),
                FieldPanel('body', ),
            ],
            heading="Basic",
        ),
        MultiFieldPanel([
            FieldPanel('carousel_style'),
            InlinePanel('carousel_items'),],heading=_('Top Images'),classname="collapsible collapsed"),              
       #/MultiFieldPanel([InlinePanel('carousel_items'),],heading=_('Images'), classname="collapsible collapsed"),
        
    ]


class RoomType2CarouselItem(Orderable):
    page = ParentalKey('wtrooms2.RoomType2Page', related_name='collection_items')
    collection = models.ForeignKey(
        'wagtailcore.Collection',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='+',
        help_text=_("The Collection to take Images from"),        
    )
    panels = [
        FieldPanel('collection'),        
    ]
    
class RoomType2PageRelatedLink(Orderable, RelatedLink):
    page = ParentalKey('RoomType2Page', related_name='related_links')    


class RoomType2PageCarouselItem(Orderable, CarouselItem):
    page = ParentalKey('RoomType2Page', related_name='side_carousel_items')

class RoomType2Page(WtBasePage):
    
    class Meta:
        verbose_name = _('Room Details Page')
    
    inline_template = 'wtrooms2/room_type2_page_inline.html'    
        
    
    page_heading = models.CharField(blank=True, max_length=255, help_text="The page outer heading")
    page_content_heading = models.CharField(blank=True, max_length=255, help_text="The page content heading")
    body = RichTextField(blank=True, help_text="Description")
    amenities = RichTextField(blank=True)
    capacity = models.CharField(blank=True, max_length=255, help_text="The Room Capacity")    
    #reservation_link = models.URLField(blank=True, null=True, max_length=255, )

    parent_page_types = ['wtrooms2.RoomType2IndexPage']
    subpage_types = []

    @property    
    def images(self):
        return self.side_carousel_items.filter(visible=True)        
    
    @property
    def thumbnail(self):
        ret = self.side_carousel_items.filter(visible=True).first()
#         if not ret:
#             ret = self.carousel_items.filter(visible=True).first()
#         if not ret:
#             ret = self.bottom_carousel_items.filter(visible=True).first()    
        
        return ret.image if ret else None

    content_panels = [
        MultiFieldPanel(
            [
                FieldPanel('title', classname="full title"),
                FieldPanel('page_heading', classname="full title"),                
                FieldPanel('page_content_heading', ),
                FieldPanel('body', ),
                FieldPanel('amenities', ),
                #FieldPanel('reservation_link', ),                                                
            ],
            heading="Basic",
        ),
        MultiFieldPanel([InlinePanel('side_carousel_items'),],heading=_('Images'), classname="collapsible "),
        MultiFieldPanel([InlinePanel('related_links'),],heading=_('Related Links'), classname="collapsible "),
    ]


